package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.ThirdForm
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ThirdFormRepository : JpaRepository<ThirdForm, Int>
