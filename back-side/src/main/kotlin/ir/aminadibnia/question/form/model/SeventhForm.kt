package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "seventh_forms")
class SeventhForm : BaseModel() {
    val table7_q1: String? = null

    val table7_q2: String? = null

    val table7_q3: String? = null

    val table7_q4: String? = null

    val table7_q5: String? = null

    val table7_q6: String? = null

    val table7_q7: String? = null

    val table7_q8: String? = null

    val table7_q9: String? = null

    val table7_q10: String? = null

    val table7_q11: String? = null

    val table7_q12: String? = null

    val table7_q13: String? = null

    val table7_q14: String? = null

    val table7_q15: String? = null

}