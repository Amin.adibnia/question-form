package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "last_priorities")
class LastPriority : BaseModel() {

    val lastPedalBike: Int? = null

    val lastSharingPedalBike: Int? = null

    val lastElectricBike: Int? = null

    val lastFoldingBike: Int? = null

    val lastScooter: Int? = null

    val lastSharingScooter: Int? = null

    val lastPersonalBikeWithPublicTransportation: Int? = null

    val lastScooterWithPublicTransportation: Int? = null

    val lastPersonalFoldingBikeWithPublicTransportation: Int? = null

    val lastThreeWheelCar: Int? = null

}