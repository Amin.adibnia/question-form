package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.GeneralQuestionForm
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface GeneralQuestionFormRepository : JpaRepository<GeneralQuestionForm, Int>