package ir.aminadibnia.question.form

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class QuestionFormApplication

fun main(args: Array<String>) {
	runApplication<QuestionFormApplication>(*args)
}


// TODO: CORS