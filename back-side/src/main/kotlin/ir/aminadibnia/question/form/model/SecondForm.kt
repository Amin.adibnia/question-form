package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "second_forms")
class SecondForm : BaseModel() {
    val table2_q1: String? = null

    val table2_q2: String? = null

    val table2_q3: String? = null

    val table2_q4: String? = null

    val table2_q5: String? = null

    val table2_q6: String? = null

    val table2_q7: String? = null

    val table2_q8: String? = null

    val table2_q9: String? = null

    val table2_q10: String? = null

    val table2_q11: String? = null

    val table2_q12: String? = null

    val table2_q13: String? = null

    val table2_q14: String? = null

    val table2_q15: String? = null
}