package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "effective_parameters_for_transportation_choices")
class EffectiveParametersForTransportationChoice : BaseModel() {

    val airConditionEffectOnTransportationChoice: Int? = null

    val paymentPowerEffectOnTransportationChoice: Int? = null

    val airPollutionEffectOnTransportationChoice: Int? = null

    val culturalEffectOnTransportationChoice: Int? = null

    val sideWalkAdaptionEffectOnTransportationChoice: Int? = null

    val suitableSpeedEffectOnTransportationChoice: Int? = null

    val harmonyEffectOnTransportationChoice: Int? = null

    val safetyEffectOnTransportationChoice: Int? = null

    val useTechnologiesEffectOnTransportationChoice: Int? = null

    val destinationDistanceEffectOnTransportationChoice: Int? = null

    val easyOfUseEffectOnTransportationChoice: Int? = null
}