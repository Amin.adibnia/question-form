package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.Responder
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ResponderRepository : JpaRepository<Responder, Int>