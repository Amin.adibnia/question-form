package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.LastComment
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LastCommentRepository : JpaRepository<LastComment, Int>