package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "third_forms")
class ThirdForm : BaseModel() {
    val table3_q1: String? = null

    val table3_q2: String? = null

    val table3_q3: String? = null

    val table3_q4: String? = null

    val table3_q5: String? = null

    val table3_q6: String? = null

    val table3_q7: String? = null

    val table3_q8: String? = null

    val table3_q9: String? = null

    val table3_q10: String? = null

    val table3_q11: String? = null

    val table3_q12: String? = null

    val table3_q13: String? = null

    val table3_q14: String? = null

    val table3_q15: String? = null

}