package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.SixthForm
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SixthFormRepository : JpaRepository<SixthForm, Int>
