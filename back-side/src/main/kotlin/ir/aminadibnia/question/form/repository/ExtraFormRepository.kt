package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.ExtraForm
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ExtraFormRepository : JpaRepository<ExtraForm, Int>