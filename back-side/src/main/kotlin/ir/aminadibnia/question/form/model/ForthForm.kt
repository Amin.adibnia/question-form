package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "forth_forms")
class ForthForm : BaseModel() {
    val table4_q1: String? = null

    val table4_q2: String? = null

    val table4_q3: String? = null

    val table4_q4: String? = null

    val table4_q5: String? = null

    val table4_q6: String? = null

    val table4_q7: String? = null

    val table4_q8: String? = null

    val table4_q9: String? = null

    val table4_q10: String? = null

    val table4_q11: String? = null

    val table4_q12: String? = null

    val table4_q13: String? = null

    val table4_q14: String? = null

    val table4_q15: String? = null

}