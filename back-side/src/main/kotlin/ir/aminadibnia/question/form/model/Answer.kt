package ir.aminadibnia.question.form.model

import com.fasterxml.jackson.annotation.JsonUnwrapped

open class Answer {

    @JsonUnwrapped
    val generalQuestionForm: GeneralQuestionForm? = null

    @JsonUnwrapped
    val responder: Responder? = null

    @JsonUnwrapped
    val dailyTrip: DailyTrip? = null

    @JsonUnwrapped
    val effectiveParametersForTransportationChoice: EffectiveParametersForTransportationChoice? = null

    @JsonUnwrapped
    val supplementaryComment: SupplementaryComment? = null

    @JsonUnwrapped
    val firstForm: FirstForm? = null

    @JsonUnwrapped
    val secondForm: SecondForm? = null

    @JsonUnwrapped
    val thirdForm: ThirdForm? = null

    @JsonUnwrapped
    val forthForm: ForthForm? = null

    @JsonUnwrapped
    val fifthForm: FifthForm? = null

    @JsonUnwrapped
    val sixthForm: SixthForm? = null

    @JsonUnwrapped
    val seventhForm: SeventhForm? = null

    @JsonUnwrapped
    val lastComment: LastComment? = null

    @JsonUnwrapped
    val lastPriority: LastPriority? = null
}