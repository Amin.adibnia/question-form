package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "extra_form")
class ExtraForm : BaseModel() {

    val tableExtra_1: String? = null

    val tableExtra_2: String? = null

    val tableExtra_3: String? = null

    val tableExtra_4: String? = null

    val tableExtra_5: String? = null

    val tableExtra_6: String? = null

    val tableExtra_7: String? = null

    val tableExtra_8: String? = null

    val tableExtra_9: String? = null

    val tableExtra_10: String? = null

    val tableExtra_11: String? = null

    val tableExtra_12: String? = null

    val tableExtra_13: String? = null

    val tableExtra_14: String? = null

    val tableExtra_15: String? = null

    val tableExtra_16: String? = null

    val tableExtra_17: String? = null

    val tableExtra_18: String? = null

    val tableExtra_19: String? = null

    val tableExtra_20: String? = null

    val tableExtra_21: String? = null
}