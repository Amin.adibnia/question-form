package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.ForthForm
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ForthFormRepository : JpaRepository<ForthForm, Int>
