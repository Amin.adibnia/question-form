package ir.aminadibnia.question.form.model

import javax.persistence.*

@Entity
@Table(name = "general_question_forms")
class GeneralQuestionForm : BaseModel() {

    val formKind: String? = null

    val formNumber: String? = null

    val airCondition: String? = null

    val questionerName: String? = null

    val questionDate: String? = null

    val questionTime: String? = null

    val questionNeighbor: String? = null

    val questionStreet: String? = null

    val questionAlley: String? = null

    val questionSquare: String? = null
}