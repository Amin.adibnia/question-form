package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "last_comments")
class LastComment : BaseModel() {
    val lastComment: String? = null
}