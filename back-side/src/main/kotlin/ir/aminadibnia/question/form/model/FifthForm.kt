package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "fifth_forms")
class FifthForm : BaseModel() {
    val table5_q1: String? = null

    val table5_q2: String? = null

    val table5_q3: String? = null

    val table5_q4: String? = null

    val table5_q5: String? = null

    val table5_q6: String? = null

    val table5_q7: String? = null

    val table5_q8: String? = null

    val table5_q9: String? = null

    val table5_q10: String? = null

    val table5_q11: String? = null

    val table5_q12: String? = null

    val table5_q13: String? = null

    val table5_q14: String? = null

    val table5_q15: String? = null

}