package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "supplementary_comments")
class SupplementaryComment : BaseModel() {

    val comment: String? = null
}