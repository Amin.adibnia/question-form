package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.FirstForm
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FirstFormRepository : JpaRepository<FirstForm, Int>
