package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.EffectiveParametersForTransportationChoice
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EffectiveParametersForTransportationChoiceRepository :
    JpaRepository<EffectiveParametersForTransportationChoice, Int>