package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table


@Entity
@Table(name = "responders")
class Responder : BaseModel() {

    val age: Int? = null

    val gender: String? = null

    val maritalStatus: String? = null

    val educationRate: String? = null

    val job: String? = null

    val familyNumber: Int? = null

    val carNumber: Int? = null

    val motorNumber: Int? = null

    val bikeNumber: Int? = null

    val carModelAndNumber: String? = null
}