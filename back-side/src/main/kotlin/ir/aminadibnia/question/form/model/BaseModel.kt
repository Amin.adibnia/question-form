package ir.aminadibnia.question.form.model

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.MappedSuperclass

@MappedSuperclass
abstract class BaseModel {
    @Id
    @GeneratedValue
    @JsonIgnore
    val id: Int? = null

//    val : String? = null
//
//    val : String? = null
//
//    val : String? = null
//
//    val : String? = null
//
//    val : String? = null
//
//    val : String? = null
//
//    val : String? = null
//
//    val : String? = null
//
//    val : String? = null
//
//    val : String? = null
}