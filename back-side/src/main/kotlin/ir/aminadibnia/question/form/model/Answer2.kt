package ir.aminadibnia.question.form.model

import com.fasterxml.jackson.annotation.JsonUnwrapped

class Answer2 : Answer() {

    @JsonUnwrapped
    val extraForm: ExtraForm? = null
}