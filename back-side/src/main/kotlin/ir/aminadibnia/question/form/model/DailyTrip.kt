package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "daily_trip")
class DailyTrip : BaseModel() {

    val originNeighbor: String? = null

    val originStreet: String? = null

    val originAlley: String? = null

    val originSquare: String? = null

    val destinationNeighbor: String? = null

    val destinationStreet: String? = null

    val destinationAlley: String? = null

    val destinationSquare: String? = null

    val purpose: String? = null

    val otherTripPurpose: String? = null

    val method: String? = null

    val combiningTripMethod: String? = null

    val tripStartHour: Int? = null

    val tripStartMin: Int? = null

    val tripMeanTime: Int? = null

    val tripDistance: Int? = null

    val publicTransport: String? = null

    val easyPublicTransport: String? = null

    val taxiPossibility: String? = null

    val easyTaxi: String? = null

    val useBike: String? = null

    val useFoldingBike: String? = null

    val useSmallerCar: String? = null

    val useScooterWitCarParked: String? = null

    val useSmallCarWitCarParked: String? = null

    val useElectricBike: String? = null

    val usePedalSharingBike: String? = null

    val useElectricScooter: String? = null

    val useElectricMotor: String? = null
}