package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "sixth_forms")
class SixthForm : BaseModel() {
    val table6_q1: String? = null

    val table6_q2: String? = null

    val table6_q3: String? = null

    val table6_q4: String? = null

    val table6_q5: String? = null

    val table6_q6: String? = null

    val table6_q7: String? = null

    val table6_q8: String? = null

    val table6_q9: String? = null

    val table6_q10: String? = null

    val table6_q11: String? = null

    val table6_q12: String? = null

    val table6_q13: String? = null

    val table6_q14: String? = null

    val table6_q15: String? = null

}