package ir.aminadibnia.question.form.config//package ir.aminadibnia.question.form
//
//import org.springframework.security.config.annotation.web.builders.HttpSecurity
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
//import org.springframework.web.cors.CorsConfiguration
//
//class SecurityConfig(
//    private val corsConfig: CorsConfiguration,
//) : WebSecurityConfigurerAdapter() {
//
//    override fun configure(http: HttpSecurity) {
//        http.csrf().disable()
//            .cors()
//            .configurationSource { corsConfig }
//            .and()
//    }
//}