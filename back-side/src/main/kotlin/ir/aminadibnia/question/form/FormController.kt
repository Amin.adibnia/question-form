package ir.aminadibnia.question.form

import ir.aminadibnia.question.form.model.Answer
import ir.aminadibnia.question.form.model.Answer2
import ir.aminadibnia.question.form.repository.*
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class FormController(
    private val generalQuestionFormRepository: GeneralQuestionFormRepository,
    private val responderRepository: ResponderRepository,
    private val dailyTripRepository: DailyTripRepository,
    private val effectiveParametersForTransportationChoiceRepository: EffectiveParametersForTransportationChoiceRepository,
    private val supplementaryCommentRepository: SupplementaryCommentRepository,
    private val firstFormRepository: FirstFormRepository,
    private val secondFormRepository: SecondFormRepository,
    private val thirdFormRepository: ThirdFormRepository,
    private val forthFormRepository: ForthFormRepository,
    private val fifthFormRepository: FifthFormRepository,
    private val sixthFormRepository: SixthFormRepository,
    private val seventhFormRepository: SeventhFormRepository,
    private val lastCommentRepository: LastCommentRepository,
    private val lastPriorityRepository: LastPriorityRepository,
    private val extraFormRepository: ExtraFormRepository,
) {

    @PostMapping("/question-form", consumes = ["application/json"])
    fun receiveAnswers(@RequestBody answer: Answer): ResponseEntity<Any> {

        generalQuestionFormRepository.save(answer.generalQuestionForm!!)
        responderRepository.save(answer.responder!!)
        dailyTripRepository.save(answer.dailyTrip!!)
        effectiveParametersForTransportationChoiceRepository.save(answer.effectiveParametersForTransportationChoice!!)
        supplementaryCommentRepository.save(answer.supplementaryComment!!)
        firstFormRepository.save(answer.firstForm!!)
        secondFormRepository.save(answer.secondForm!!)
        thirdFormRepository.save(answer.thirdForm!!)
        forthFormRepository.save(answer.forthForm!!)
        fifthFormRepository.save(answer.fifthForm!!)
        sixthFormRepository.save(answer.sixthForm!!)
        seventhFormRepository.save(answer.seventhForm!!)

        lastCommentRepository.save(answer.lastComment!!)
        lastPriorityRepository.save(answer.lastPriority!!)

        return ResponseEntity(HttpStatus.OK)
    }

    @PostMapping("/question-form-2", consumes = ["application/json"])
    fun receiveAnswers(@RequestBody answer: Answer2): ResponseEntity<Any> {

        generalQuestionFormRepository.save(answer.generalQuestionForm!!)
        responderRepository.save(answer.responder!!)
        dailyTripRepository.save(answer.dailyTrip!!)
        effectiveParametersForTransportationChoiceRepository.save(answer.effectiveParametersForTransportationChoice!!)
        supplementaryCommentRepository.save(answer.supplementaryComment!!)

        firstFormRepository.save(answer.firstForm!!)
        secondFormRepository.save(answer.secondForm!!)
        thirdFormRepository.save(answer.thirdForm!!)
        forthFormRepository.save(answer.forthForm!!)
        fifthFormRepository.save(answer.fifthForm!!)
        sixthFormRepository.save(answer.sixthForm!!)
        seventhFormRepository.save(answer.seventhForm!!)

        lastCommentRepository.save(answer.lastComment!!)
        lastPriorityRepository.save(answer.lastPriority!!)

        extraFormRepository.save(answer.extraForm!!)

        return ResponseEntity(HttpStatus.OK)
    }
}