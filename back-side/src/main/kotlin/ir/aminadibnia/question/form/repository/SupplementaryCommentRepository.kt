package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.SupplementaryComment
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SupplementaryCommentRepository : JpaRepository<SupplementaryComment, Int>