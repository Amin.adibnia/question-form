package ir.aminadibnia.question.form.model

import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "first_forms")
class FirstForm : BaseModel() {

    val pedalBikeElectricBike: String? = null

    val pedalBikeFoldingPedalBike: String? = null

    val pedalBikeElectricMotor: String? = null

    val table1_q4: String? = null

    val table1_q5: String? = null

    val table1_q6: String? = null

    val table1_q7: String? = null

    val table1_q8: String? = null

    val table1_q9: String? = null

    val table1_q10: String? = null

    val table1_q11: String? = null

    val table1_q12: String? = null

    val table1_q13: String? = null

    val table1_q14: String? = null

    val table1_q15: String? = null
}