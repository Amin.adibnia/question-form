package ir.aminadibnia.question.form.repository

import ir.aminadibnia.question.form.model.LastPriority
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface LastPriorityRepository : JpaRepository<LastPriority, Int>