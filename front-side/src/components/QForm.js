import React from "react";
import "./citizen.css";
import axios from "axios";
import img1 from './../assets/1.jpg'
import img2 from './../assets/2.png'
import img3 from './../assets/3.jpg'
import img4 from './../assets/4.jpg'
import img5 from './../assets/5.png'
import img6 from './../assets/6.png'
import img7 from './../assets/7.png'
import img8 from './../assets/8.jpg'
import img9 from './../assets/9.jpg'
import img10 from './../assets/10.jpg'
import img11 from './../assets/11.png'
import img12 from './../assets/12.png'
import img13 from './../assets/13.png'
import img14 from './../assets/14.png'
import img15 from './../assets/15.png'
import img16 from './../assets/16.jpg'
import img17 from './../assets/17.jpg'
import img18 from './../assets/18.png'
import img19 from './../assets/19.jpg'
import img20 from './../assets/20.png'
import img21 from './../assets/21.png'
import img22 from './../assets/22.png'
import img23 from './../assets/23.png'
import img24 from './../assets/24.png'
import img25 from './../assets/25.png'
import img26 from './../assets/26.png'
import img27 from './../assets/27.png'
import img28 from './../assets/28.png'
import img29 from './../assets/29.jpg'
import img30 from './../assets/30.jpg'
import img31 from './../assets/31.png'
import img32 from './../assets/32.jpg'
import img33 from './../assets/33.jpg'


class Citizen extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            formKind: String(null),
            formNumber: String(null),
            airCondition: String(null),
            questionerName: String(null),
            questionDate: null,
            questionTime: null,
            // todo: 2 date
            questionNeighbor: String(null),
            questionStreet: String(null),
            questionAlley: String(null),
            questionSquare: String(null),

            age: null,
            gender: null,
            maritalStatus: null,
            educationRate: null,
            job: null,
            familyNumber: null,
            carNumber: null,
            motorNumber: null,
            bikeNumber: null,
            carModelAndNumber: null,

            originNeighbor: null,
            originStreet: null,
            originAlley: null,
            originSquare: null,
            destinationNeighbor: null,
            destinationStreet: null,
            destinationAlley: null,
            destinationSquare: null,
            purpose: null,
            otherTripPurpose: null,
            method: null,
            combiningTripMethod: null,
            tripStartHour: null,
            tripStartMin: null,
            tripMeanTime: null,
            tripDistance: null,
            publicTransport: null,
            easyPublicTransport: null,
            taxiPossibility: null,
            easyTaxi: null,
            useBike: null,
            useFoldingBike: null,
            useSmallerCar: null,
            useScooterWitCarParked: null,
            useSmallCarWitCarParked: null,
            useElectricBike: null,
            usePedalSharingBike: null,
            useElectricScooter: null,
            useElectricMotor: null,
        

            airConditionEffectOnTransportationChoice: null,
            paymentPowerEffectOnTransportationChoice: null,
            airPollutionEffectOnTransportationChoice: null,
            culturalEffectOnTransportationChoice: null,
            sideWalkAdaptionEffectOnTransportationChoice: null,
            suitableSpeedEffectOnTransportationChoice: null,
            harmonyEffectOnTransportationChoice: null,
            safetyEffectOnTransportationChoice: null,
            useTechnologiesEffectOnTransportationChoice: null,
            destinationDistanceEffectOnTransportationChoice: null,
            easyOfUseEffectOnTransportationChoice: null,


            comment: null,




            tableExtra_1: null,

            tableExtra_2: null,
        
            tableExtra_3: null,
        
            tableExtra_4: null,
        
            tableExtra_5: null,
        
            tableExtra_6: null,
        
            tableExtra_7: null,
        
            tableExtra_8: null,
        
            tableExtra_9: null,
        
            tableExtra_10: null,
        
            tableExtra_11: null,
        
            tableExtra_12: null,
        
            tableExtra_13: null,
        
            tableExtra_14: null,
        
            tableExtra_15: null,
        
            tableExtra_16: null,
        
            tableExtra_17: null,
        
            tableExtra_18: null,
        
            tableExtra_19: null,
        
            tableExtra_20: null,
        
            tableExtra_21: null,
        





    pedalBikeElectricBike: null,
    pedalBikeFoldingPedalBike: null,
    pedalBikeElectricMotor: null,
    table1_q4: null,
    table1_q5: null,
    table1_q6: null,
    table1_q7: null,
    table1_q8: null,
    table1_q9: null,
    table1_q10: null,
    table1_q11: null,
    table1_q12: null,
    table1_q13: null,
    table1_q14: null,
    table1_q15: null,



    table2_q1: null,
    table2_q2: null,
    table2_q3: null,
    table2_q4: null,
    table2_q5: null,
    table2_q6: null,
    table2_q7: null,
    table2_q8: null,
    table2_q9: null,
    table2_q10: null,
    table2_q11: null,
    table2_q12: null,
    table2_q13: null,
    table2_q14: null,
    table2_q15: null,




    table3_q1: null,
    table3_q2: null,
    table3_q3: null,
    table3_q4: null,
    table3_q5: null,
    table3_q6: null,
    table3_q7: null,
    table3_q8: null,
    table3_q9: null,
    table3_q10: null,
    table3_q11: null,
    table3_q12: null,
    table3_q13: null,
    table3_q14: null,
    table3_q15: null,


    table4_q1: null,
    table4_q2: null,
    table4_q3: null,
    table4_q4: null,
    table4_q5: null,
    table4_q6: null,
    table4_q7: null,
    table4_q8: null,
    table4_q9: null,
    table4_q10: null,
    table4_q11: null,
    table4_q12: null,
    table4_q13: null,
    table4_q14: null,
    table4_q15: null,


    table5_q1: null,
    table5_q2: null,
    table5_q3: null,
    table5_q4: null,
    table5_q5: null,
    table5_q6: null,
    table5_q7: null,
    table5_q8: null,
    table5_q9: null,
    table5_q10: null,
    table5_q11: null,
    table5_q12: null,
    table5_q13: null,
    table5_q14: null,
    table5_q15: null,


    table6_q1: null,
    table6_q2: null,
    table6_q3: null,
    table6_q4: null,
    table6_q5: null,
    table6_q6: null,
    table6_q7: null,
    table6_q8: null,
    table6_q9: null,
    table6_q10: null,
    table6_q11: null,
    table6_q12: null,
    table6_q13: null,
    table6_q14: null,
    table6_q15: null,


    table7_q1: null,
    table7_q2: null,
    table7_q3: null,
    table7_q4: null,
    table7_q5: null,
    table7_q6: null,
    table7_q7: null,
    table7_q8: null,
    table7_q9: null,
    table7_q10: null,
    table7_q11: null,
    table7_q12: null,
    table7_q13: null,
    table7_q14: null,
    table7_q15: null,


    lastComment: null,



    lastPedalBike:null, 
    lastSharingPedalBike:null,
    lastElectricBike:null, 
    lastFoldingBike:null,
    lastScooter:null,
    lastSharingScooter:null,
    lastPersonalBikeWithPublicTransportation:null,
    lastScooterWithPublicTransportation:null,
    lastPersonalFoldingBikeWithPublicTransportation:null,
    lastThreeWheelCar: null

      };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.faToEn = this.faToEn.bind(this);
    }
 
    
    input_name = {
      "formKind": "formKind",
      "formNumber": "formNumber",
      "airCondition": "airCondition",
      "questionerName": "questionerName",
      // todo: 2 date
      "questionNeighbor": "questionNeighbor",
      "questionStreet": "questionStreet",
      "questionAlley": "questionAlley",
      "questionSquare": "questionSquare",

      "age": "age",
      "gender": "gender",
      "maritalStatus": "maritalStatus",
      "educationRate": "educationRate",
      "job": "job",
      "familyNumber": "familyNumber",
      "carNumber": "carNumber",
      "motorNumber": "motorNumber",
      "bikeNumber": "bikeNumber",
      "carModelAndNumber": "carModelAndNumber",

      "originNeighbor": "originNeighbor",
      "originStreet": "originStreet",
      "originAlley": "originAlley",
      "originSquare": "originSquare",
      "destinationNeighbor": "destinationNeighbor",
      "destinationStreet": "destinationStreet",
      "destinationAlley": "destinationAlley",
      "destinationSquare": "destinationSquare",
      "purpose": "purpose",
      "otherTripPurpose": "otherTripPurpose",
      "method": "method",
      "combiningTripMethod": "combiningTripMethod",
      // : null,
      // : null,
      // : null,
      // : null,
      // : null,

}


    handleChange(event) {
      console.log(this.state.gender);
      console.log(event.target.name);
      console.log(event.target.value);

      if (event.target.name === "formKind")
        this.setState({
          formKind: event.target.value,
        });
      else if (event.target.name === "formNumber")
      this.setState({
        formNumber: event.target.value,
      });
      else if (event.target.name === "airCondition")
      this.setState({
        airCondition: event.target.value,
      });
      else if (event.target.name === "questionerName")
      this.setState({
        questionerName: event.target.value
      })
      else if (event.target.name === "questionDate")
      this.setState({
        questionDate: event.target.value,
      });
      else if (event.target.name === "questionTime")
      this.setState({
        questionTime: event.target.value,
      });
      // todo: 2 date
      else if (event.target.name === "questionNeighbor")
      this.setState({
        questionNeighbor: event.target.value,
      });
      else if (event.target.name === "questionStreet")
      this.setState({
        questionStreet: event.target.value,
      });
      else if (event.target.name === "questionAlley")
      this.setState({
        questionAlley: event.target.value
      })
      else if (event.target.name === "questionSquare")
      this.setState({
        questionSquare: event.target.value,
      });



      else if (event.target.name === "age")
      this.setState({
        age: event.target.value,
      });
      else if (event.target.name === "gender")
      this.setState({
        gender: event.target.value
      })
      else if (event.target.name === "maritalStatus")
      this.setState({
        maritalStatus: event.target.value,
      });
      else if (event.target.name === "educationRate")
      this.setState({
        educationRate: event.target.value,
      });
      else if (event.target.name === "job")
      this.setState({
        job: event.target.value
      })
      else if (event.target.name === "familyNumber")
      this.setState({
        familyNumber: event.target.value,
      });
      else if (event.target.name === "carNumber")
      this.setState({
        carNumber: event.target.value,
      });
      else if (event.target.name === "motorNumber")
      this.setState({
        motorNumber: event.target.value
      })
      else if (event.target.name === "bikeNumber")
      this.setState({
        bikeNumber: event.target.value,
      });
      else if (event.target.name === "carModelAndNumber")
      this.setState({
        carModelAndNumber: event.target.value,
      });



      else if (event.target.name === "originNeighbor")
      this.setState({
        originNeighbor: event.target.value
      })
      else if (event.target.name === "originStreet")
      this.setState({
        originStreet: event.target.value,
      });
      else if (event.target.name === "originAlley")
      this.setState({
        originAlley: event.target.value,
      });
      else if (event.target.name === "originSquare")
      this.setState({
        originSquare: event.target.value
      })
      else if (event.target.name === "destinationNeighbor")
      this.setState({
        destinationNeighbor: event.target.value,
      });
      else if (event.target.name === "destinationStreet")
      this.setState({
        destinationStreet: event.target.value,
      });
      else if (event.target.name === "destinationAlley")
      this.setState({
        destinationAlley: event.target.value
      })
      else if (event.target.name === "destinationSquare")
      this.setState({
        destinationSquare: event.target.value,
      });
      else if (event.target.name === "purpose")
      this.setState({
        purpose: event.target.value,
      });
      else if (event.target.name === "otherTripPurpose")
      this.setState({
        otherTripPurpose: event.target.value
      })
      else if (event.target.name === "method")
      this.setState({
        method: event.target.value,
      });
      else if (event.target.name === "combiningTripMethod")
      this.setState({
        combiningTripMethod: event.target.value,
      });
      else if (event.target.name === "tripStartHour")
      this.setState({
        tripStartHour: event.target.value
      })
      else if (event.target.name === "tripStartMin")
      this.setState({
        tripStartMin: event.target.value,
      });
      else if (event.target.name === "tripMeanTime")
      this.setState({
        tripMeanTime: event.target.value,
      });
      else if (event.target.name === "tripDistance")
      this.setState({
        tripDistance: event.target.value
      })
      else if (event.target.name === "publicTransport")
      this.setState({
        publicTransport: event.target.value,
      });
      else if (event.target.name === "easyPublicTransport")
      this.setState({
        easyPublicTransport: event.target.value,
      });
      else if (event.target.name === "taxiPossibility")
      this.setState({
        taxiPossibility: event.target.value
      })
      else if (event.target.name === "easyTaxi")
      this.setState({
        easyTaxi: event.target.value,
      });
      else if (event.target.name === "useBike")
      this.setState({
        useBike: event.target.value,
      });
      else if (event.target.name === "useFoldingBike")
      this.setState({
        useFoldingBike: event.target.value
      })
      else if (event.target.name === "useSmallerCar")
      this.setState({
        useSmallerCar: event.target.value,
      });
      else if (event.target.name === "useScooterWitCarParked")
      this.setState({
        useScooterWitCarParked: event.target.value,
      });
      else if (event.target.name === "useSmallCarWitCarParked")
      this.setState({
        useSmallCarWitCarParked: event.target.value
      })
      else if (event.target.name === "usePedalSharingBike")
      this.setState({
        usePedalSharingBike: event.target.value,
      });
      else if (event.target.name === "useElectricScooter")
      this.setState({
        useElectricScooter: event.target.value,
      });
      else if (event.target.name === "useElectricMotor")
      this.setState({
        useElectricMotor: event.target.value
      })


      
      else if (event.target.name === "airConditionEffectOnTransportationChoice")
      this.setState({
        airConditionEffectOnTransportationChoice: event.target.value,
      });
      else if (event.target.name === "paymentPowerEffectOnTransportationChoice")
      this.setState({
        paymentPowerEffectOnTransportationChoice: event.target.value,
      });
      else if (event.target.name === "airPollutionEffectOnTransportationChoice")
      this.setState({
        airPollutionEffectOnTransportationChoice: event.target.value
      })
      else if (event.target.name === "culturalEffectOnTransportationChoice")
      this.setState({
        culturalEffectOnTransportationChoice: event.target.value,
      });
      else if (event.target.name === "sideWalkAdaptionEffectOnTransportationChoice")
      this.setState({
        sideWalkAdaptionEffectOnTransportationChoice: event.target.value,
      });
      else if (event.target.name === "suitableSpeedEffectOnTransportationChoice")
      this.setState({
        suitableSpeedEffectOnTransportationChoice: event.target.value
      })
      else if (event.target.name === "harmonyEffectOnTransportationChoice")
      this.setState({
        harmonyEffectOnTransportationChoice: event.target.value,
      });
      else if (event.target.name === "safetyEffectOnTransportationChoice")
      this.setState({
        safetyEffectOnTransportationChoice: event.target.value,
      });
      else if (event.target.name === "useTechnologiesEffectOnTransportationChoice")
      this.setState({
        useTechnologiesEffectOnTransportationChoice: event.target.value,
      });
      else if (event.target.name === "destinationDistanceEffectOnTransportationChoice")
      this.setState({
        destinationDistanceEffectOnTransportationChoice: event.target.value
      })
      else if (event.target.name === "easyOfUseEffectOnTransportationChoice")
      this.setState({
        easyOfUseEffectOnTransportationChoice: event.target.value,
      });



      else if (event.target.name === "comment")
      this.setState({
        comment: event.target.value,
      });




      else if (event.target.name === "tableExtra_1")
      this.setState({
        tableExtra_1: event.target.value,
      });
      else if (event.target.name === "tableExtra_2")
      this.setState({
        tableExtra_2: event.target.value,
      });
      else if (event.target.name === "tableExtra_3")
      this.setState({
        tableExtra_3: event.target.value
      })
      else if (event.target.name === "tableExtra_4")
      this.setState({
        tableExtra_4: event.target.value,
      });
      else if (event.target.name === "tableExtra_5")
      this.setState({
        tableExtra_5: event.target.value,
      });
      else if (event.target.name === "tableExtra_6")
      this.setState({
        tableExtra_6: event.target.value
      })
      else if (event.target.name === "tableExtra_7")
      this.setState({
        tableExtra_7: event.target.value,
      });
      else if (event.target.name === "tableExtra_8")
      this.setState({
        tableExtra_8: event.target.value,
      });
      else if (event.target.name === "tableExtra_9")
      this.setState({
        tableExtra_9: event.target.value,
      });
      else if (event.target.name === "tableExtra_10")
      this.setState({
        tableExtra_10: event.target.value
      })
      else if (event.target.name === "tableExtra_11")
      this.setState({
        tableExtra_11: event.target.value,
      });
      else if (event.target.name === "tableExtra_12")
      this.setState({
        tableExtra_12: event.target.value,
      });
      else if (event.target.name === "tableExtra_13")
      this.setState({
        tableExtra_13: event.target.value,
      });
      else if (event.target.name === "tableExtra_14")
      this.setState({
        tableExtra_14: event.target.value,
      });
      else if (event.target.name === "tableExtra_15")
      this.setState({
        tableExtra_15: event.target.value,
      });
      else if (event.target.name === "tableExtra_16")
      this.setState({
        tableExtra_16: event.target.value,
      });
      else if (event.target.name === "tableExtra_17")
      this.setState({
        tableExtra_17: event.target.value
      })
      else if (event.target.name === "tableExtra_18")
      this.setState({
        tableExtra_18: event.target.value,
      });
      else if (event.target.name === "tableExtra_19")
      this.setState({
        tableExtra_19: event.target.value,
      });
      else if (event.target.name === "tableExtra_20")
      this.setState({
        tableExtra_20: event.target.value,
      });
      else if (event.target.name === "tableExtra_21")
      this.setState({
        tableExtra_21: event.target.value,
      });





      else if (event.target.name === "pedalBikeElectricBike")
      this.setState({
        pedalBikeElectricBike: event.target.value,
      });
      else if (event.target.name === "pedalBikeFoldingPedalBike")
      this.setState({
        pedalBikeFoldingPedalBike: event.target.value,
      });
      else if (event.target.name === "pedalBikeElectricMotor")
      this.setState({
        pedalBikeElectricMotor: event.target.value
      })
      else if (event.target.name === "table1_q4")
      this.setState({
        table1_q4: event.target.value,
      });
      else if (event.target.name === "table1_q5")
      this.setState({
        table1_q5: event.target.value,
      });
      else if (event.target.name === "table1_q6")
      this.setState({
        table1_q6: event.target.value
      })
      else if (event.target.name === "table1_q7")
      this.setState({
        table1_q7: event.target.value,
      });
      else if (event.target.name === "table1_q8")
      this.setState({
        table1_q8: event.target.value,
      });
      else if (event.target.name === "table1_q9")
      this.setState({
        table1_q9: event.target.value,
      });
      else if (event.target.name === "table1_q10")
      this.setState({
        table1_q10: event.target.value
      })
      else if (event.target.name === "table1_q11")
      this.setState({
        table1_q11: event.target.value,
      });
      else if (event.target.name === "table1_q12")
      this.setState({
        table1_q12: event.target.value,
      });
      else if (event.target.name === "table1_q13")
      this.setState({
        table1_q13: event.target.value,
      });
      else if (event.target.name === "table1_q14")
      this.setState({
        table1_q14: event.target.value,
      });
      else if (event.target.name === "table1_q15")
      this.setState({
        table1_q15: event.target.value,
      });



      else if (event.target.name === "table2_q1")
      this.setState({
        table2_q1: event.target.value,
      });
      else if (event.target.name === "table2_q2")
      this.setState({
        table2_q2: event.target.value,
      });
      else if (event.target.name === "table2_q3")
      this.setState({
        table2_q3: event.target.value
      })
      else if (event.target.name === "table2_q4")
      this.setState({
        table2_q4: event.target.value,
      });
      else if (event.target.name === "table2_q5")
      this.setState({
        table2_q5: event.target.value,
      });
      else if (event.target.name === "table2_q6")
      this.setState({
        table2_q6: event.target.value
      })
      else if (event.target.name === "table2_q7")
      this.setState({
        table2_q7: event.target.value,
      });
      else if (event.target.name === "table2_q8")
      this.setState({
        table2_q8: event.target.value,
      });
      else if (event.target.name === "table2_q9")
      this.setState({
        table2_q9: event.target.value,
      });
      else if (event.target.name === "table2_q10")
      this.setState({
        table2_q10: event.target.value
      })
      else if (event.target.name === "table2_q11")
      this.setState({
        table2_q11: event.target.value,
      });
      else if (event.target.name === "table2_q12")
      this.setState({
        table2_q12: event.target.value,
      });
      else if (event.target.name === "table2_q13")
      this.setState({
        table2_q13: event.target.value,
      });
      else if (event.target.name === "table2_q14")
      this.setState({
        table2_q14: event.target.value,
      });
      else if (event.target.name === "table2_q15")
      this.setState({
        table2_q15: event.target.value,
      });


      else if (event.target.name === "table3_q1")
      this.setState({
        table3_q1: event.target.value,
      });
      else if (event.target.name === "table3_q2")
      this.setState({
        table3_q2: event.target.value,
      });
      else if (event.target.name === "table3_q3")
      this.setState({
        table3_q3: event.target.value
      })
      else if (event.target.name === "table3_q4")
      this.setState({
        table3_q4: event.target.value,
      });
      else if (event.target.name === "table3_q5")
      this.setState({
        table3_q5: event.target.value,
      });
      else if (event.target.name === "table3_q6")
      this.setState({
        table3_q6: event.target.value
      })
      else if (event.target.name === "table3_q7")
      this.setState({
        table3_q7: event.target.value,
      });
      else if (event.target.name === "table3_q8")
      this.setState({
        table3_q8: event.target.value,
      });
      else if (event.target.name === "table3_q9")
      this.setState({
        table3_q9: event.target.value,
      });
      else if (event.target.name === "table3_q10")
      this.setState({
        table3_q10: event.target.value
      })
      else if (event.target.name === "table3_q11")
      this.setState({
        table3_q11: event.target.value,
      });
      else if (event.target.name === "table3_q12")
      this.setState({
        table3_q12: event.target.value,
      });
      else if (event.target.name === "table3_q13")
      this.setState({
        table3_q13: event.target.value,
      });
      else if (event.target.name === "table3_q14")
      this.setState({
        table3_q14: event.target.value,
      });
      else if (event.target.name === "table3_q15")
      this.setState({
        table3_q15: event.target.value,
      });



      else if (event.target.name === "table4_q1")
      this.setState({
        table4_q1: event.target.value,
      });
      else if (event.target.name === "table4_q2")
      this.setState({
        table4_q2: event.target.value,
      });
      else if (event.target.name === "table4_q3")
      this.setState({
        table4_q3: event.target.value
      })
      else if (event.target.name === "table4_q4")
      this.setState({
        table4_q4: event.target.value,
      });
      else if (event.target.name === "table4_q5")
      this.setState({
        table4_q5: event.target.value,
      });
      else if (event.target.name === "table4_q6")
      this.setState({
        table4_q6: event.target.value
      })
      else if (event.target.name === "table4_q7")
      this.setState({
        table4_q7: event.target.value,
      });
      else if (event.target.name === "table4_q8")
      this.setState({
        table4_q8: event.target.value,
      });
      else if (event.target.name === "table4_q9")
      this.setState({
        table4_q9: event.target.value,
      });
      else if (event.target.name === "table4_q10")
      this.setState({
        table4_q10: event.target.value
      })
      else if (event.target.name === "table4_q11")
      this.setState({
        table4_q11: event.target.value,
      });
      else if (event.target.name === "table4_q12")
      this.setState({
        table4_q12: event.target.value,
      });
      else if (event.target.name === "table4_q13")
      this.setState({
        table4_q13: event.target.value,
      });
      else if (event.target.name === "table4_q14")
      this.setState({
        table4_q14: event.target.value,
      });
      else if (event.target.name === "table4_q15")
      this.setState({
        table4_q15: event.target.value,
      });



      else if (event.target.name === "table5_q1")
      this.setState({
        table5_q1: event.target.value,
      });
      else if (event.target.name === "table5_q2")
      this.setState({
        table5_q2: event.target.value,
      });
      else if (event.target.name === "table5_q3")
      this.setState({
        table5_q3: event.target.value
      })
      else if (event.target.name === "table5_q4")
      this.setState({
        table5_q4: event.target.value,
      });
      else if (event.target.name === "table5_q5")
      this.setState({
        table5_q5: event.target.value,
      });
      else if (event.target.name === "table5_q6")
      this.setState({
        table5_q6: event.target.value
      })
      else if (event.target.name === "table5_q7")
      this.setState({
        table5_q7: event.target.value,
      });
      else if (event.target.name === "table5_q8")
      this.setState({
        table5_q8: event.target.value,
      });
      else if (event.target.name === "table5_q9")
      this.setState({
        table5_q9: event.target.value,
      });
      else if (event.target.name === "table5_q10")
      this.setState({
        table5_q10: event.target.value
      })
      else if (event.target.name === "table5_q11")
      this.setState({
        table5_q11: event.target.value,
      });
      else if (event.target.name === "table5_q12")
      this.setState({
        table5_q12: event.target.value,
      });
      else if (event.target.name === "table5_q13")
      this.setState({
        table5_q13: event.target.value,
      });
      else if (event.target.name === "table5_q14")
      this.setState({
        table5_q14: event.target.value,
      });
      else if (event.target.name === "table5_q15")
      this.setState({
        table5_q15: event.target.value,
      });




      else if (event.target.name === "table6_q1")
      this.setState({
        table6_q1: event.target.value,
      });
      else if (event.target.name === "table6_q2")
      this.setState({
        table6_q2: event.target.value,
      });
      else if (event.target.name === "table6_q3")
      this.setState({
        table6_q3: event.target.value
      })
      else if (event.target.name === "table6_q4")
      this.setState({
        table6_q4: event.target.value,
      });
      else if (event.target.name === "table6_q5")
      this.setState({
        table6_q5: event.target.value,
      });
      else if (event.target.name === "table6_q6")
      this.setState({
        table6_q6: event.target.value
      })
      else if (event.target.name === "table6_q7")
      this.setState({
        table6_q7: event.target.value,
      });
      else if (event.target.name === "table6_q8")
      this.setState({
        table6_q8: event.target.value,
      });
      else if (event.target.name === "table6_q9")
      this.setState({
        table6_q9: event.target.value,
      });
      else if (event.target.name === "table6_q10")
      this.setState({
        table6_q10: event.target.value
      })
      else if (event.target.name === "table6_q11")
      this.setState({
        table6_q11: event.target.value,
      });
      else if (event.target.name === "table6_q12")
      this.setState({
        table6_q12: event.target.value,
      });
      else if (event.target.name === "table6_q13")
      this.setState({
        table6_q13: event.target.value,
      });
      else if (event.target.name === "table6_q14")
      this.setState({
        table6_q14: event.target.value,
      });
      else if (event.target.name === "table6_q15")
      this.setState({
        table6_q15: event.target.value,
      });




      else if (event.target.name === "table7_q1")
      this.setState({
        table7_q1: event.target.value,
      });
      else if (event.target.name === "table7_q2")
      this.setState({
        table7_q2: event.target.value,
      });
      else if (event.target.name === "table7_q3")
      this.setState({
        table7_q3: event.target.value
      })
      else if (event.target.name === "table7_q4")
      this.setState({
        table7_q4: event.target.value,
      });
      else if (event.target.name === "table7_q5")
      this.setState({
        table7_q5: event.target.value,
      });
      else if (event.target.name === "table7_q6")
      this.setState({
        table7_q6: event.target.value
      })
      else if (event.target.name === "table7_q7")
      this.setState({
        table7_q7: event.target.value,
      });
      else if (event.target.name === "table7_q8")
      this.setState({
        table7_q8: event.target.value,
      });
      else if (event.target.name === "table7_q9")
      this.setState({
        table7_q9: event.target.value,
      });
      else if (event.target.name === "table7_q10")
      this.setState({
        table7_q10: event.target.value
      })
      else if (event.target.name === "table7_q11")
      this.setState({
        table7_q11: event.target.value,
      });
      else if (event.target.name === "table7_q12")
      this.setState({
        table7_q12: event.target.value,
      });
      else if (event.target.name === "table7_q13")
      this.setState({
        table7_q13: event.target.value,
      });
      else if (event.target.name === "table7_q14")
      this.setState({
        table7_q14: event.target.value,
      });
      else if (event.target.name === "table7_q15")
      this.setState({
        table7_q15: event.target.value,
      });


      else if (event.target.name === "lastComment")
      this.setState({
        lastComment: event.target.value
      })



      else if (event.target.name === "lastPedalBike")
      this.setState({
        lastPedalBike: event.target.value,
      });
      else if (event.target.name === "lastSharingPedalBike")
      this.setState({
        lastSharingPedalBike: event.target.value,
      });
      else if (event.target.name === "lastElectricBike")
      this.setState({
        lastElectricBike: event.target.value
      })
      else if (event.target.name === "lastFoldingBike")
      this.setState({
        lastFoldingBike: event.target.value,
      });
      else if (event.target.name === "lastScooter")
      this.setState({
        lastScooter: event.target.value,
      });
      else if (event.target.name === "lastSharingScooter")
      this.setState({
        lastSharingScooter: event.target.value,
      });
      else if (event.target.name === "lastPersonalBikeWithPublicTransportation")
      this.setState({
        lastPersonalBikeWithPublicTransportation: event.target.value
      })
      else if (event.target.name === "lastScooterWithPublicTransportation")
      this.setState({
        lastScooterWithPublicTransportation: event.target.value,
      });
      else if (event.target.name === "lastPersonalFoldingBikeWithPublicTransportation")
      this.setState({
        lastPersonalFoldingBikeWithPublicTransportation: event.target.value,
      });
      else if (event.target.name === "lastThreeWheelCar")
      this.setState({
        lastThreeWheelCar: event.target.value,
      });


      
    }

    makeData() {
      return {
        formKind: this.state.formKind,
        formNumber: this.state.formNumber,
        airCondition: this.state.airCondition,
        questionerName: this.state.questionerName,
        questionDate: this.state.questionDate,
        questionTime: this.state.questionTime,
        // todo: 2 date
        questionNeighbor: this.state.questionNeighbor,
        questionStreet: this.state.questionStreet,
        questionAlley: this.state.questionAlley,
        questionSquare: this.state.questionSquare,

        age: this.state.age,
        gender: this.state.gender,
        maritalStatus: this.state.maritalStatus,
        educationRate: this.state.educationRate,
        job: this.state.job,
        familyNumber: this.state.familyNumber,
        carNumber: this.state.carNumber,
        motorNumber: this.state.motorNumber,
        bikeNumber: this.state.bikeNumber,
        carModelAndNumber: this.state.carModelAndNumber,

        originNeighbor: this.state.originNeighbor,
        originStreet: this.state.originStreet,
        originAlley: this.state.originAlley,
        originSquare: this.state.originSquare,
        destinationNeighbor: this.state.destinationNeighbor,
        destinationStreet: this.state.destinationStreet,
        destinationAlley: this.state.destinationAlley,
        destinationSquare: this.state.destinationSquare,
        purpose: this.state.purpose,
        otherTripPurpose: this.state.otherTripPurpose,
        method: this.state.method,
        combiningTripMethod: this.state.combiningTripMethod,
        tripStartHour: this.state.tripStartHour,
        tripStartMin: this.state.tripStartMin,
        tripMeanTime: this.state.tripMeanTime,
        tripDistance: this.state.tripDistance,
        publicTransport: this.state.publicTransport,
        easyPublicTransport: this.state.easyPublicTransport,
        taxiPossibility: this.state.taxiPossibility,
        easyTaxi: this.state.easyTaxi,
        useBike: this.state.useBike,
        useFoldingBike: this.state.useFoldingBike,
        useSmallerCar: this.state.useSmallerCar,
        useScooterWitCarParked: this.state.useScooterWitCarParked,
        useSmallCarWitCarParked: this.state.useSmallCarWitCarParked,
        useElectricBike: this.state.useElectricBike,
        usePedalSharingBike: this.state.usePedalSharingBike,
        useElectricScooter: this.state.useElectricScooter,
        useElectricMotor: this.state.useElectricMotor,
    

        airConditionEffectOnTransportationChoice: this.state.airConditionEffectOnTransportationChoice,
        paymentPowerEffectOnTransportationChoice: this.state.paymentPowerEffectOnTransportationChoice,
        airPollutionEffectOnTransportationChoice: this.state.airPollutionEffectOnTransportationChoice,
        culturalEffectOnTransportationChoice: this.state.culturalEffectOnTransportationChoice,
        sideWalkAdaptionEffectOnTransportationChoice: this.state.sideWalkAdaptionEffectOnTransportationChoice,
        suitableSpeedEffectOnTransportationChoice: this.state.suitableSpeedEffectOnTransportationChoice,
        harmonyEffectOnTransportationChoice: this.state.harmonyEffectOnTransportationChoice,
        safetyEffectOnTransportationChoice: this.state.safetyEffectOnTransportationChoice,
        useTechnologiesEffectOnTransportationChoice: this.state.useTechnologiesEffectOnTransportationChoice,
        destinationDistanceEffectOnTransportationChoice: this.state.destinationDistanceEffectOnTransportationChoice,
        easyOfUseEffectOnTransportationChoice: this.state.easyOfUseEffectOnTransportationChoice,


        comment: this.state.comment,




        tableExtra_1: this.state.tableExtra_1,
        tableExtra_2: this.state.tableExtra_2,
        tableExtra_3: this.state.tableExtra_3,
        tableExtra_4: this.state.tableExtra_4,
        tableExtra_5: this.state.tableExtra_5,
        tableExtra_6: this.state.tableExtra_6,
        tableExtra_7: this.state.tableExtra_7,
        tableExtra_8: this.state.tableExtra_8,
        tableExtra_9: this.state.tableExtra_9,
        tableExtra_10: this.state.tableExtra_10,
        tableExtra_11: this.state.tableExtra_11,
        tableExtra_12: this.state.tableExtra_12,
        tableExtra_13: this.state.tableExtra_13,
        tableExtra_14: this.state.tableExtra_14,
        tableExtra_15: this.state.tableExtra_15,
        tableExtra_16: this.state.tableExtra_16,
        tableExtra_17: this.state.tableExtra_17,
        tableExtra_18: this.state.tableExtra_18,
        tableExtra_19: this.state.tableExtra_19,
        tableExtra_20: this.state.tableExtra_20,    
        tableExtra_21: this.state.tableExtra_21,
    



pedalBikeElectricBike: this.state.pedalBikeElectricBike,
pedalBikeFoldingPedalBike: this.state.pedalBikeFoldingPedalBike,
pedalBikeElectricMotor: this.state.pedalBikeElectricMotor,
table1_q4: this.state.table1_q4,
table1_q5: this.state.table1_q5,
table1_q6: this.table1_q6,
table1_q7: this.state.table1_q7,
table1_q8: this.state.table1_q8,
table1_q9: this.state.table1_q9,
table1_q10: this.state.table1_q10,
table1_q11: this.state.table1_q11,
table1_q12: this.state.table1_q12,
table1_q13: this.state.table1_q13,
table1_q14: this.state.table1_q14,
table1_q15: this.state.table1_q15,



table2_q1: this.state.table2_q1,
table2_q2: this.state.table2_q2,
table2_q3: this.state.table2_q3,
table2_q4: this.state.table2_q4,
table2_q5: this.state.table2_q5,
table2_q6: this.state.table2_q6,
table2_q7: this.state.table2_q7,
table2_q8: this.state.table2_q8,
table2_q9: this.state.table2_q9,
table2_q10: this.state.table2_q10,
table2_q11: this.state.table2_q11,
table2_q12: this.state.table2_q12,
table2_q13: this.state.table2_q13,
table2_q14: this.state.table2_q14,
table2_q15: this.state.table2_q15,




table3_q1: this.state.table3_q1,
table3_q2: this.state.table3_q2,
table3_q3: this.state.table3_q3,
table3_q4: this.state.table3_q4,
table3_q5: this.state.table3_q5,
table3_q6: this.state.table3_q6,
table3_q7: this.state.table3_q7,
table3_q8: this.state.table3_q8,
table3_q9: this.state.table3_q9,
table3_q10: this.state.table3_q10,
table3_q11: this.state.table3_q11,
table3_q12: this.state.table3_q12,
table3_q13: this.state.table3_q13,
table3_q14: this.state.table3_q14,
table3_q15: this.state.table3_q15,



table4_q1: this.state.table4_q1,
table4_q2: this.state.table4_q2,
table4_q3: this.state.table4_q3,
table4_q4: this.state.table4_q4,
table4_q5: this.state.table4_q5,
table4_q6: this.state.table4_q6,
table4_q7: this.state.table4_q7,
table4_q8: this.state.table4_q8,
table4_q9: this.state.table4_q9,
table4_q10: this.state.table4_q10,
table4_q11: this.state.table4_q11,
table4_q12: this.state.table4_q12,
table4_q13: this.state.table4_q13,
table4_q14: this.state.table4_q14,
table4_q15: this.state.table4_q15,



table5_q1: this.state.table5_q1,
table5_q2: this.state.table5_q2,
table5_q3: this.state.table5_q3,
table5_q4: this.state.table5_q4,
table5_q5: this.state.table5_q5,
table5_q6: this.state.table5_q6,
table5_q7: this.state.table5_q7,
table5_q8: this.state.table5_q8,
table5_q9: this.state.table5_q9,
table5_q10: this.state.table5_q10,
table5_q11: this.state.table5_q11,
table5_q12: this.state.table5_q12,
table5_q13: this.state.table5_q13,
table5_q14: this.state.table5_q14,
table5_q15: this.state.table5_q15,




table6_q1: this.state.table6_q1,
table6_q2: this.state.table6_q2,
table6_q3: this.state.table6_q3,
table6_q4: this.state.table6_q4,
table6_q5: this.state.table6_q5,
table6_q6: this.state.table6_q6,
table6_q7: this.state.table6_q7,
table6_q8: this.state.table6_q8,
table6_q9: this.state.table6_q9,
table6_q10: this.state.table6_q10,
table6_q11: this.state.table6_q11,
table6_q12: this.state.table6_q12,
table6_q13: this.state.table6_q13,
table6_q14: this.state.table6_q14,
table6_q15: this.state.table6_q15,



table7_q1: this.state.table7_q1,
table7_q2: this.state.table7_q2,
table7_q3: this.state.table7_q3,
table7_q4: this.state.table7_q4,
table7_q5: this.state.table7_q5,
table7_q6: this.state.table7_q6,
table7_q7: this.state.table7_q7,
table7_q8: this.state.table7_q8,
table7_q9: this.state.table7_q9,
table7_q10: this.state.table7_q10,
table7_q11: this.state.table7_q11,
table7_q12: this.state.table7_q12,
table7_q13: this.state.table7_q13,
table7_q14: this.state.table7_q14,
table7_q15: this.state.table7_q15,




lastComment: this.state.lastComment,



lastPedalBike:this.state.lastPedalBike, 
lastSharingPedalBike:this.state.lastSharingPedalBike,
lastElectricBike:this.state.lastElectricBike, 
lastFoldingBike:this.state.lastFoldingBike,
lastScooter:this.state.lastScooter,
lastSharingScooter:this.state.lastSharingScooter,
lastPersonalBikeWithPublicTransportation:this.state.lastPersonalBikeWithPublicTransportation,
lastScooterWithPublicTransportation:this.state.lastScooterWithPublicTransportation,
lastPersonalFoldingBikeWithPublicTransportation:this.state.lastPersonalFoldingBikeWithPublicTransportation,
lastThreeWheelCar: this.state.lastThreeWheelCar,


       }
    }

 
      

    renderTextInput = (name) => {
        return (
            <input name={name} onChange={this.handleChange} />
            //value={this.state[name]}
        )
    }

    renderBigTextInput = (name) => {
      return (
        <textarea name={name} onChange={this.handleChange} rows="8" cols="200" />
      );
    }

    renderRadio = (name, val) => {
        return(
          // <label>
          //   val
            <input type="radio" name={name} value={val} onClick={this.handleChange} />
          // </label>
        );
    }

    handleSubmit(event) {
        axios({
            method: "post",
            url: "http://localhost:8080/question-form-2",
            data: this.makeData(),
            headers: { "Content-Type": "application/json" },
        })
            .then(function (response) {
              //handle success
              alert("ممنون از همکاری شما :)")
            })
            .catch(function (response) {
              //handle error
              alert("Error...")
            });
              event.preventDefault();
    }

    renderSubmitButton = () => {
        return (
            <div>
                <input type="submit" value="وارد کردن اطلاعات" />
            </div>
        )
    }


    faToEn(str) {
      for (var i = 0; i < str.size; i++) {
        if (str[i] == '\u06f0') str[i] = '0';
        else if (str[i] == '\u06f1') str[i] = '1'; 
        else if (str[i] == '\u06f2') str[i] = '2'; 
        else if (str[i] == '\u06f3') str[i] = '3'; 
        else if (str[i] == '\u06f4') str[i] = '4'; 
        else if (str[i] == '\u06f5') str[i] = '5'; 
        else if (str[i] == '\u06f6') str[i] = '6'; 
        else if (str[i] == '\u06f7') str[i] = '7'; 
        else if (str[i] == '\u06f8') str[i] = '8'; 
        else if (str[i] == '\u06f9') str[i] = '9'; 
 
      }
    }


  
    render() {
        return (
<div id="main-div">
            <meta httpEquiv="content-type" content="text/html; charset=utf-8" />
            <meta name="generator" content="LibreOffice 6.4.7.2 (Linux)" />
            <meta name="author" content="Maadized PC" />
            <meta name="created" content="2021-08-24T08:26:00" />
            <meta name="changed" content="2021-08-29T20:41:18.907313854" />
            <meta name="AppVersion" content={16.0} />
            <meta name="DocSecurity" content={0} />
            <meta name="HyperlinksChanged" content="false" />
            <meta name="LinksUpToDate" content="false" />
            <meta name="ScaleCrop" content="false" />
            <meta name="ShareDoc" content="false" />
            <style
              type="text/css"
              dangerouslySetInnerHTML={{
                __html:
                  "\n\t\t@page { size: 8.5in 11in; margin: 0.33in }\n\t\tp { margin-bottom: 0.1in; direction: rtl; line-height: 115%; text-align: left; orphans: 2; widows: 2; background: transparent;        text-align: right;\n }\n        table, td, th {\n          border: 1px solid black;\n        text-align: right;\n        }\n\n        table {\n          border-collapse: collapse;\n          width: 100%;\n        }\n\n        th {\n          height: 70px;\n        }\n\n\t\ta:link { color: #000080; text-decoration: underline }\n\t\ta.western:link { so-language: zxx }\n\t\ta.cjk:link { so-language: zxx }\n\t\ta:visited { color: #800000; text-decoration: underline }\n\t\ta.western:visited { so-language: zxx }\n\t\ta.cjk:visited { so-language: zxx }\n\t"
              }}
            />

<form onSubmit={this.handleSubmit}>

{this.faToEn("۳۳")}

            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
              className="center-text"
            >
              <font size={5} style={{ fontSize: "18pt" }}>
                <b> </b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={5} style={{ fontSize: "18pt" }}>
                      <b>بسم تعالی</b>
                    </font>
                  </font>
                </span>
              </font>
            </p>
            <p
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
              className="center-text"
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "16pt" }}>
                      <b>پرسشنامه شماره</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "16pt" }}>
                <b>(1)</b>
              </font>
            </p>
            <p
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>پرسشنامه</b>
                    </font>
                  </font>
                </span>
              </font>
            </p>
            <table dir="rtl" width={623} cellPadding={7} cellSpacing={0}>
              <colgroup>
                <col width={193} />
                <col width={194} />
                <col width={193} />
              </colgroup>
              <tbody>
                <tr valign="top">
                  <td
                    width={193}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">نوع پرسشنامه</font>
                        </span>
                      </font>

                                                                              {this.renderTextInput("formKind")}

                    </p>
                  </td>
                  <td
                    width={194}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">شماره پرسشنامه</font>
                        </span>
                      </font>


                                                                              {this.renderTextInput("formNumber")}



                    </p>
                  </td>
                  <td
                    width={193}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">شرایط جوی در زمان پرسشگری</font>
                        </span>
                      </font>

                                                                                    {this.renderTextInput("airCondition")}


                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={193}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">نام پرسشگر</font>
                        </span>
                      </font>
                                                                                      {this.renderTextInput("questionerName")}
                    </p>
                  </td>
                  <td
                    width={194}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">تاریخ پرسشگری</font>
                        </span>
                      </font>
                      {this.renderTextInput("questionDate")}
                    </p>
                  </td>
                  <td
                    width={193}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">زمان پرسشگری</font>
                        </span>
                      </font>
                      {this.renderTextInput("questionTime")}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    colSpan={3}
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">مکان پرسشگری</font>
                        </span>
                      </font>
                      :{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">محله</font>
                        </span>
                      </font>
                                                                    {this.renderTextInput("questionNeighbor")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">خیابان اصلی</font>
                        </span>
                      </font>
                                                                {this.renderTextInput("questionStreet")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">خیابان فرعی</font>
                        </span>
                      </font>
                                                                         {this.renderTextInput("questionAlley")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">نام تقاطع، میدان یا ساختمان مشهور</font>
                        </span>
                      </font>
                                                                            {this.renderTextInput("questionSquare")}
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
            <p
              dir="rtl"
              align="justify"
              style={{ textIndent: "0.5in", marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="justify"
              style={{ textIndent: "0.5in", marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      شهروند گرامی، این پرسشنامه به منظور تعیین وزن شاخص های موثر در
                      شناسایی عوامل موثردر انتخاب گونه‌های حمل‌ونقلی مناسب در بافت‌های
                      تاریخی براساس روش
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                AHP (
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      فرایند تحلیل سلسله مراتبی
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                )
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      طراحی شده است
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                .
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      در ابتدا قسمتی از سوالات مربوط به مشخصات فرد مورد پرسش، مشخصات سفر
                      روزانه، گونه‌های حمل‌ونقل و برخی از عوامل موثر در انتخاب گونه های
                      حمل‌ونقلی است{" "}
                    </font>
                  </font>
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      و پس از آن نظرات و پیشنهادهای پرسش‌دهنده در مورد عوامل موثر در
                      انتخاب گونه‌های حمل‌ونقلی مناسب در بافت تاریخی است و نهایت مقایسه
                      زوجی عوامل آورده‌شده است
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">.</span>
              </font>
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="justify"
              style={{ textIndent: "0.5in", marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>(</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>الف</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>)-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>مشخصات‌</b>
                    </font>
                  </font>
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>فرد مورد پرسش </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <table dir="rtl" width={623} cellPadding={7} cellSpacing={0}>
              <colgroup>
                <col width={248} />
                <col width={138} />
                <col width={193} />
              </colgroup>
              <tbody>
                <tr valign="top">
                  <td
                    width={248}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      1)
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">سن</font>
                        </span>
                      </font>
                                                                                {this.renderTextInput("age")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">سال</font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={138}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      2){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">جنسیت</font>
                        </span>
                      </font>
                      :
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">مرد</font>
                                                                                    {this.renderRadio("gender", "man")}
                          
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">زن</font>
                                                                                      {this.renderRadio("gender", "woman")}

                          {/* <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={193}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      3){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">وضعیت تاهل</font>
                        </span>
                      </font>
                      :{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">مجرد</font>
                          {/* <font face="Times New Roman">□</font> */}

                                                                {this.renderRadio("maritalStatus", "single")}

                          <font face="B Nazanin">متاهل</font>
                          {/* <font face="Times New Roman">□</font> */}

                                                                  {this.renderRadio("maritalStatus", "married")}

                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    colSpan={3}
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      4){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">میزان تحصیلات</font>
                        </span>
                      </font>
                      :{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">زیر دیپلم</font>
                                                                        {this.renderRadio("educationRate", "highSchool")}
                        </span>
                      </font>
                      :<font face="Times New Roman, serif"></font>
                      {/* <font face="Times New Roman, serif">□</font> */}
                      <font face="Times New Roman, serif"></font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">دیپلم و فوق‌دیپلم</font>
                                               {this.renderRadio("educationRate", "diplomaAndPost")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">لیسانس</font>
                                                                                    {this.renderRadio("educationRate", "bachelor")}

                          {/* <font face="Times New Roman">□</font>  */}
                          <font face="B Nazanin">فوق لیسانس</font>
                          <font face="B Nazanin">پزشک عمومی/</font>
                                                                                             {this.renderRadio("educationRate", "postBachelor")}

                        </span>
                      </font>
                      <font face="Times New Roman, serif">/</font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">دکترا</font>
                          <font face="B Nazanin">پزشک متخصص/</font>
                          {/* <font face="Times New Roman">□</font> */}
                                                                        {this.renderRadio("educationRate", "doctorate")}
                        </span>
                      </font>
                      <font face="Times New Roman, serif">/</font>
                      <font face="Arial">
                        <span lang="fa-IR">
                                                                      {/* {this.renderRadio("educationRate", "specialistDoctor")} */}
                          {/* <font face="B Nazanin">پزشک متخصص</font> */}
                          {/* <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    colSpan={3}
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      5){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">شغل</font>
                        </span>
                      </font>
                      :
                      <font face="Arial">
                        <span lang="fa-IR">
                        <font face="B Nazanin">کارمند</font>
                                                                            {this.renderRadio("job", "emplyee")}

                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">شغل آزاد</font>
                                                                            {this.renderRadio("job", "freelance")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">دانش‌آموز</font>
                                                                            {this.renderRadio("job", "schoolStudent")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">دانشجو</font>
                                                                            {this.renderRadio("job", "uniStudent")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خانه‌دار</font>
                                                                                                      {this.renderRadio("job", "houseHolder")}

                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">بازنشسته</font>
                                                                                                      {this.renderRadio("job", "retired")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">پزشک</font>
                        </span>
                      </font>
                      <font face="Times New Roman, serif">/</font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">استاد دانشگاه/</font>
                          {this.renderRadio("job", "doctorOrUniTeacher")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">بیکار</font>
                          {this.renderRadio("job", "noWork")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">سایر</font>
                                                                              {this.renderRadio("job", "other")}

                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={248}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      6){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">تعداد اعضای خانواده</font>
                        </span>
                      </font>
                                                                              {this.renderTextInput("familyNumber")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">نفر</font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    colSpan={2}
                    width={345}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      7){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">تعداد خودروهای در مالکیت خانواده</font>
                        </span>
                      </font>
                                                                                      {this.renderTextInput("carNumber")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">دستگاه</font>
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={248}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      8){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">تعداد موتورسیکلت در مالکیت خانواده</font>
                        </span>
                      </font>
                                                                          {this.renderTextInput("motorNumber")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">دستگاه</font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    colSpan={2}
                    width={345}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      9){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">تعداد دوچرخه در مالکیت خانواده</font>
                        </span>
                      </font>
                                                                                  {this.renderTextInput("bikeNumber")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">دستگاه</font>
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    colSpan={3}
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify" style={{ marginBottom: "0in" }}>
                      10){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            نوع و سال ساخت خودروهای در مالکیت خانوار
                          </font>
                        </span>
                      </font>
                      :
                                                                    {this.renderTextInput("carModelAndNumber")}
                    </p>
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={2} style={{ fontSize: "10pt" }}>
                              مثال
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={2} style={{ fontSize: "10pt" }}>
                        :
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={2} style={{ fontSize: "10pt" }}>
                              پراید، مدل ۱۳۹۲
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
            <p style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <br />
              <br />
            </p>
            <p style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <br />
              <br />
            </p>
            <p style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <br />
              <br />
            </p>
            <p style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <br />
              <br />
            </p>
            <p style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={4} style={{ fontSize: "16pt" }}>
                <b>(</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "16pt" }}>
                      <b>ب</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "16pt" }}>
                <b>)-</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "16pt" }}>
                      <b>راهنمای تصویری پرسشنامه </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "16pt" }}>
                <b>:</b>
              </font>
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>الف</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>نمونه دوچرخه پدالی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img1}
                name="Shape1"
                alt="Shape1"
                align="bottom"
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img2}
                name="Picture 26"
                align="bottom"
                width={328}
                height={286}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              {/* <img
                src={img3}
                name="Picture 27"
                align="bottom"
                width={256}
                height={256}
                border={0}
              /> */}
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>ب</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>نمونه دوچرخه تاشو</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img3}
                name="Picture 2"
                align="bottom"
                width={427}
                height={284}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
              <img
                src={img4}
                name="Picture 1"
                align="bottom"
                width={311}
                height={207}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}
              />
              <img
                src={img5}
                name="Image1"
                align="bottom"
                width={578}
                height={321}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}
              />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>پ</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>نمونه دوچرخه برقی </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img6}
                name="Picture 3"
                align="bottom"
                width={485}
                height={323}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img7}
                name="Picture 4"
                align="bottom"
                width={470}
                height={432}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>ت</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>نمونه اسکوتر برقی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img8}
                name="Picture 5"
                align="bottom"
                width={301}
                height={222}
                border={0}
              />
              <img
                src={img9}
                name="Picture 6"
                align="left"
                hspace={12}
                width={283}
                height={214}
                border={0}
              />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img10}
                name="Picture 7"
                align="bottom"
                width={431}
                height={233}
                border={0}
              />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img11}
                name="Image2"
                align="bottom"
                width={439}
                height={254}
                border={0}
              />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>ث</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>حمل دوچرخه با حمل‌ونقل همگانی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">:</span>
              </font>
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img12}
                name="Picture 9"
                align="bottom"
                width={624}
                height={205}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={4} style={{ fontSize: "16pt" }}>
                <span lang="en-US"> </span>
              </font>
              <img
                src={img13}
                name="Picture 10"
                align="bottom"
                width={624}
                height={207}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={4} style={{ fontSize: "16pt" }}>
                <span lang="en-US"> </span>
              </font>
              <img
                src={img14}
                name="Picture 11"
                align="bottom"
                width={624}
                height={210}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>ج</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>حمل اسکوتر با حمل‌ونقل همگانی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img15}
                name="Picture 12"
                align="left"
                hspace={12}
                width={308}
                height={278}
                border={0}
              />
              <br />
              <br />
            </p>
            <p align="center" style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <img
                src={img16}
                name="Picture 13"
                align="bottom"
                width={320}
                height={250}
                border={0}
              />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img17}
                name="Picture 17"
                align="bottom"
                width={624}
                height={351}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>د</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>حمل دوچرخه تاشو با حمل‌ونقل همگانی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img18}
                name="Picture 15"
                align="bottom"
                width={624}
                height={240}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img19}
                name="Picture 18"
                align="bottom"
                width={624}
                height={468}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>چ</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>نمونه خودروهای سه‌چرخ و چهار‌چرخ برقی کوچک</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="left"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="left"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              {/* <img
                src={img20}
                name="Image3"
                align="bottom"
                width={286}
                height={215}
                border={0}
              /> */}
              <img
                src={img20}
                name="Picture 19"
                align="left"
                width={204}
                height={207}
                border={0}
              />
            </p>
            <br />
            <br></br>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img22}
                name="Image4"
                align="bottom"
                width={415}
                height={279}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img23}
                name="Image5"
                align="bottom"
                width={276}
                height={207}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}
              />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>ح</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>نمونه خودروهای همگانی مخصوص بافت تاریخی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="center"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <img
                src={img24}
                name="Picture 16"
                align="bottom"
                width={335}
                height={325}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
              <img
                src={img25}
                name="Picture 14"
                align="bottom"
                width={350}
                height={314}
                border={0}
                style={{
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                
                }}

              />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>(</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>ج</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>)-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>مشخصات</b>
                    </font>
                  </font>
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>اصلی ترین سفر روزانه شما</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <table dir="rtl" width={623} cellPadding={7} cellSpacing={0}>
              <colgroup>
                <col width={607} />
              </colgroup>
              <tbody>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      1)
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">مبدا سفر</font>
                        </span>
                      </font>
                      :{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">محله</font>
                        </span>
                      </font>
                                                                              {this.renderTextInput("originNeighbor")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">خیابان اصلی</font>
                        </span>
                      </font>
                                                                                      {this.renderTextInput("originStreet")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">خیابان فرعی</font>
                        </span>
                      </font>
                                                                                      {this.renderTextInput("originAlley")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">نام تقاطع، میدان یا ساختمان مشهور</font>
                        </span>
                      </font>
                                                                                      {this.renderTextInput("originSquare")}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      2){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">مقصد سفر</font>
                        </span>
                      </font>
                      :{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">محله</font>
                        </span>
                      </font>
                                                                        {this.renderTextInput("destinationNeighbor")}

                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">خیابان اصلی</font>
                        </span>
                      </font>
                                                           {this.renderTextInput("destinationStreet")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">خیابان فرعی</font>
                        </span>
                      </font>
                                                {this.renderTextInput("destinationAlley")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">نام تقاطع، میدان یا ساختمان مشهور</font>
                        </span>
                      </font>
                                    {this.renderTextInput("destinationSquare")}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify" style={{ marginBottom: "0in" }}>
                      3){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">منظور</font>
                        </span>
                      </font>
                      (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">هدف</font>
                        </span>
                      </font>
                      )
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">اصلی ترین سفر روزانه شما </font>
                        </span>
                      </font>
                      :
                      <font face="Arial">
                        <span lang="fa-IR">

                          <font face="B Nazanin">کاری</font>
                          {this.renderRadio("purpose", "work")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">تحصیلی</font>
                          {this.renderRadio("purpose", "education")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خرید</font>
                          {this.renderRadio("purpose", "selling")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">تفریح</font>
                          {this.renderRadio("purpose", "entertainment")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">کارشخصی</font>
                          {this.renderRadio("purpose", "personal")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">سایر</font>
                          {this.renderRadio("purpose", "other")}
                          {/* <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            در صورت تمایل اگر گزینه‌ی سایر را انتخاب نموده‌اید، هدف از
                            سفرتان را ذکر کنید
                          </font>
                        </span>
                      </font>
                      <font face="Times New Roman, serif">
                      {this.renderTextInput("otherTripPurpose")}
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify" style={{ marginBottom: "0in" }}>
                      <span lang="en-US">4)</span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">شیوه انجام این سفر</font>
                        </span>
                      </font>
                      :
                    </p>
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">پیاده</font>
                          {this.renderRadio("method", "walking")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">دوچرخه</font>
                          {this.renderRadio("method", "bike")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">موتورسیکلت</font>
                          {this.renderRadio("method", "motor")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خودرو شخصی</font>
                          {this.renderRadio("method", "car")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">اتوبوس</font>
                          {this.renderRadio("method", "bus")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">تاکسی</font>

                        </span>
                      </font>
                      /
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin"> اسنپ </font>
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin"> ترکیبی </font>
                          {this.renderRadio("method", "taxi")}
                        </span>
                      </font>
                      : (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">شیوه ها ثبت شوند</font>
                        </span>
                      </font>
                      {this.renderTextInput("combiningTripMethod")}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      5){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            معمولا زمان شروع این سفر شما چه زمانی است؟
                          </font>
                        </span>
                      </font>
                      {this.renderTextInput("tripStartHour")}
:                      {this.renderTextInput("tripStartMin")}
(
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">ساعت</font>
                        </span>
                      </font>
                      :
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">دقیقه</font>
                        </span>
                      </font>
                      )
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      6)
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">متوسط زمان سفر شما چقدر است؟</font>
                        </span>
                      </font>
                                                       {this.renderTextInput("tripMeanTime")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">دقیقه</font>
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      7)
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">طول یا مسافت سفر</font>
                        </span>
                      </font>
                      (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">بعدا توسط محقق تخمین زده شود</font>
                        </span>
                      </font>
                                          {this.renderTextInput("tripDistance")}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">کیلومتر</font>
                        </span>
                      </font>
                      )
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify" style={{ marginBottom: "0in" }}>
                      8)
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">آیا در طول این سفر شما</font>
                        </span>
                      </font>
                      (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">یا قسمتی از آن</font>
                        </span>
                      </font>
                      ){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            امکان استفاده از حمل و نقل همگانی وجود دارد؟ بله
                          </font>
                          {/* <font face="Times New Roman">□</font> */}
                          {this.renderRadio("publicTransport", "yes")}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("publicTransport", "no")}
                          {/* <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            آیا استفاده از حمل و نقل همگانی نسبتا راحت است؟ بله
                          </font>
                          {this.renderRadio("easyPublicTransport", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("easyPublicTransport", "no")}
                          {/* <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify" style={{ marginBottom: "0in" }}>
                      9){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">آیا در طول این سفر شما </font>
                        </span>
                      </font>
                      (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">یا قسمتی از آن</font>
                        </span>
                      </font>
                      ){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            امکان استفاده از تاکسی، ون، تاکسی اینترنتی
                          </font>
                        </span>
                      </font>
                      (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">شبه حمل‌ونقل عمومی مانند اسنپ</font>
                        </span>
                      </font>
                      )
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">وجود دارد؟ بله</font>

                          {this.renderRadio("taxiPossibility", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("taxiPossibility", "no")}


                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            آیا استفاده از آنها نسبتا راحت است؟ بله
                          </font>
                          {this.renderRadio("easyTaxi", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("easyTaxi", "no")}

                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">10)</span>{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            در صورتی که شرایطی فراهم شود که بتوانید دوچرخه‌ی شخصی داشته و
                            آن را در طول سفر با حمل‌ونقل همگانی با خود همراه کنید، آیا
                            تمایلی به استفاده از این شیوه حمل‌ونقل ترکیبی را خواهید داشت
                          </font>
                          <font face="B Nazanin">؟</font>
                          <font face="B Nazanin">بله</font>
                          {this.renderRadio("useBike", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("useBike", "no")}

                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      11){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            در صورتی که شرایطی فراهم شود که بتوانید اسکوتر شخصی پدالی یا
                            برقی تهیه کنید و آن را در طول سفر با حمل‌ونقل همگانی با خود
                            همراه کنید، آیا تمایلی به استفاده از این شیوه حمل و نقل ترکیبی
                            را خواهید داشت؟ بله
                          </font>
                          {this.renderRadio("useScooter", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("useScooter", "no")}
                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      12)
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            در صورتی که شرایطی فراهم شود که بتوانید دوچرخه تاشو شخصی تهیه
                            کنید و آن را در طول سفر با حمل‌ونقل همگانی با خود همراه کنید،
                            آیا تمایلی به استفاده از این شیوه حمل و نقل ترکیبی را خواهید
                            داشت؟ بله
                          </font>
                          {this.renderRadio("useFoldingBike", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("useFoldingBike", "no")}
                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify" style={{ marginBottom: "0in" }}>
                      <span lang="en-US">13)</span>{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            در صورتی که شرایطی فراهم شود که بتوانید از خودرو کم سرنشین و
                            کوچکتر{" "}
                          </font>
                        </span>
                      </font>
                      (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">سه چرخ و چهار چرخ برقی کوچک</font>
                        </span>
                      </font>
                      )
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            برای سفر های خود در داخل و بیرون محدوده بافت تاریخی استفاده
                            کنید، آیا تمایلی به تهیه و استفاده از این گونه حمل‌ونقل را
                            خواهید‌داشت؟{" "}
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="justify">
                      {" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">بله</font>
                          {this.renderRadio("useSmallerCar", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("useSmallerCar", "no")}

                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">14)</span>{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            چنانچه خودرو شخصی دارید، در صورتی که شرایطی فراهم شود که
                            بتوانید خودرو شخصی تان را در یک پارکینگ عمومی نزدیک به محدوده
                            بافت تاریخی پارک نموده و سفر خود را با استفاده از دوچرخه یا
                            اسکوتر اشتراکی
                          </font>
                        </span>
                      </font>
                      (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">کرایه ای</font>
                        </span>
                      </font>
                      ){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            در داخل بافت ادامه دهید، آیا تمایلی به استفاده از این شیوه
                            حمل‌ونقل ترکیبی را خواهید‌داشت؟ بله
                          </font>
                          {this.renderRadio("useSharingBike", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("useSharingBike", "no")}
                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font> */}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خودرو شخصی ندارم</font>
                          {this.renderRadio("useSharingBike", "carProblem")}
                          {/* <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">15)</span>{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            چنانچه خودرو شخصی دارید، در صورتی که شرایطی فراهم شود که
                            بتوانید خودرو شخصی تان را در یک پارکینگ عمومی نزدیک به محدوده
                            بافت تاریخی پارک نموده و سفر خود را با استفاده از دوچرخه یا
                            اسکوتر شخصی تهیه شده توسط خود ادامه دهید، آیا تمایلی به
                            استفاده از این شیوه حمل‌ونقل ترکیبی را خواهید‌داشت؟ بله
                          </font>
                          {this.renderRadio("useScooterWitCarParked", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("useScooterWitCarParked", "no")}
                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font> */}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خودرو شخصی ندارم</font>
                          {this.renderRadio("useScooterWitCarParked", "carProblem")}
                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خودرو شخصی ندارم</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">16)</span>{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            چنانچه خودرو شخصی دارید، در صورتی که شرایطی فراهم شود که
                            بتوانید خودرو شخصی تان را در یک پارکینگ عمومی نزدیک به محدوده
                            بافت تاریخی پارک نموده و سفر خود در داخل بافت را با استفاده از
                            خوردوهای همگانی کوچک مخصوص بافت تاریخی ادامه دهید، آیا تمایلی
                            به استفاده از این گونه حمل‌و‌نقل ترکیبی را خواهید داشت؟ بله
                          </font>
                          {this.renderRadio("useSmallCarWitCarParked", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("useSmallCarWitCarParked", "no")}

                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">17)</span>{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            در صورتی که شرایطی فراهم شود که بتوانید از دوچرخه برقی برای
                            سفر های روزانه خود
                          </font>
                        </span>
                      </font>
                      (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">داخل و خارج بافت</font>
                        </span>
                      </font>
                      ){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            استفاده کنید، آیا تمایلی به تهیه و استفاده از این شیوه
                            حمل‌ونقل را خواهید‌داشت؟ بله
                          </font>
                          {this.renderRadio("useElectricBike", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("useElectricBike", "no")}
                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font> */}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خودرو شخصی ندارم</font>
                          {this.renderRadio("useElectricBike", "carProblem")}

                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خودرو شخصی ندارم</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">18)</span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            آیا تمایل دارید بخشی از سفرهای روزانه خود را با دوچرخه پدالی
                            شخصی انجام دهید؟
                          </font>
                          <font face="B Nazanin">بله</font>
                          {this.renderRadio("usePedalBike", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("usePedalBike", "no")}

                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">19)</span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            آیا تمایل دارید بخشی از سفرهای روزانه خود را با دوچرخه پدالی
                            اشتراکی انجام دهید ؟
                          </font>
                          <font face="B Nazanin"> بله</font>
                          {this.renderRadio("usePedalSharingBike", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("usePedalSharingBike", "no")}
                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">20)</span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            آیا تمایل دارید بخشی از سفرهای روزانه خود را با اسکوتر برقی
                            انجام دهید؟
                          </font>
                          <font face="B Nazanin">بله</font>
                          {this.renderRadio("useElectricScooter", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("useElectricScooter", "no")}

                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr>
                  <td
                    width={607}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">21)</span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            آیا تمایل دارید برای سفرهای روزانه خود از موتورسیکلت برقی کم
                            سرعت شهری استفاده کنید؟
                          </font>
                          <font face="B Nazanin"> بله</font>
                          {this.renderRadio("useElectricMotor", "yes")}
                          {/* <font face="Times New Roman">□</font> */}
                          <font face="B Nazanin">خیر</font>
                          {this.renderRadio("useElectricMotor", "no")}

                          {/* <font face="Times New Roman">□</font>
                          <font face="B Nazanin">خیر</font>
                          <font face="Times New Roman">□</font> */}
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
            <p
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={4} style={{ fontSize: "14pt" }}>
                <b>(</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>د</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <b>)-</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>عوامل موثر در انتخاب گونه‌های حمل‌ونقلی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <b>:</b>
              </font>
            </p>
            <table dir="rtl" width={624} cellPadding={7} cellSpacing={0}>
              <colgroup>
                <col width={477} />
                <col width={117} />
              </colgroup>
              <tbody>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="center">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>برخی از عوامل موثر در انتخاب گونه‌های حمل‌ونقلی</b>
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>امتیازدهی</b>
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={4} style={{ fontSize: "14pt" }}>
                        <span lang="en-US">
                          <b>(</b>
                        </span>
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>از</b>
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={4} style={{ fontSize: "14pt" }}>
                        <span lang="en-US">
                          <b>0</b>
                        </span>
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>تا</b>
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={4} style={{ fontSize: "14pt" }}>
                        <span lang="en-US">
                          <b>10)</b>
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">1) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            به نظر شما تا چه اندازه عامل شرایط‌جوی و{" "}
                          </font>
                          <font face="B Nazanin">
                            آب‌وهوایی منطقه مورد نظر بر روی انتخاب گونه حمل‌ونقلی اثر
                            دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                              {this.renderTextInput("airConditionEffectOnTransportationChoice")}
                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">2) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            توان پرداخت هزینه خرید وسیله گونه مورد نظر اگر شخصی باشد بر
                            روی انتخاب گونه حمل‌ونقلی اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                            {this.renderTextInput("paymentPowerEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            توان پرداخت هزینه های استفاده از گونه مورد نظر بر روی انتخاب
                            گونه حمل‌ونقلی اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">3) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            میزان آلودگی تولید‌شده توسط گونه حمل‌ونقلی مورد نظر بر روی
                            انتخاب گونه حمل‌ونقلی اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                            {this.renderTextInput("airPollutionEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">4) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            زمینه فرهنگی، میزان پذیرش و آمادگی مردم آن محله و منطقه مورد
                            نظر بر روی انتخاب گونه حمل‌ونقلی اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                                  {this.renderTextInput("culturalEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">5) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            محدودیت استفاده از گونه‌های خاص توسط بانوان و افراد دارای
                            محدودیت فیزیکی
                          </font>
                        </span>
                      </font>
                      (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">افراد سالمند، معلولین، کودکان و </font>
                        </span>
                      </font>
                      ....)
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            و همچنین توان جسمی و فیزیکی افراد بر روی انتخاب گونه حمل‌ونقلی
                            اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                              {this.renderTextInput("womenLimitationEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">6) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            قابلیت انطباق آن گونه حمل‌ونقلی با مشخصات
                          </font>
                        </span>
                      </font>
                      (
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">ساختار فیزیکی</font>
                        </span>
                      </font>
                      ){" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            شبکه معابر داخل بافت مورد نظر بر روی انتخاب گونه حمل‌ونقلی اثر
                            دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                            {this.renderTextInput("sideWalkAdaptionEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">7) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            سرعت جابجایی مناسب با در نظر گرفتن محدودیت‌های داخل بافت مورد
                            نظر بر روی انتخاب گونه حمل‌ونقلی اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                {this.renderTextInput("suitableSpeedEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">8) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            هماهنگی و به‌حداقل رساندن اختلال و آسیب گونه حمل‌ونقلی با
                            ویژگی‌های بافت‌تاریخی، برروی انتخاب گونه حمل‌ونقلی اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                                      {this.renderTextInput("harmonyEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">9)</span>{" "}
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            ایمن بودن گونه حمل‌ونقلی مورد نظر بر روی انتخاب گونه حمل‌ونقلی
                            اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                                {this.renderTextInput("safetyEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">10) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            استفاده از فناوری‌های روز دنیا بمنظور هوشمندسازی جابجایی سفر
                            در بافت تاریخی بر روی انتخاب گونه حمل‌ونقلی اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                        {this.renderTextInput("useTechnologiesEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">11) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه عامل </font>
                          <font face="B Nazanin">
                            فاصله تا مقصد مورد نظر بر روی انتخاب گونه حمل‌ونقلی اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                            {this.renderTextInput("destinationDistanceEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
                <tr valign="top">
                  <td
                    width={477}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <span lang="en-US">12) </span>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">به نظر شما تا چه اندازه </font>
                          <font face="B Nazanin">
                            سهولت و راحتی استفاده از گونه حمل‌ونقلی بر روی انتخاب گونه
                            حمل‌ونقلی اثر دارد؟
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                  <td
                    width={117}
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p lang="en-US" align="justify">
                                            {this.renderTextInput("easyOfUseEffectOnTransportationChoice")}

                      <br />
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
            <p
              lang="en-US"
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>(</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>ه</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>)-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>نظرات تکمیلی و پیشنهادهای فرد مورد پرسش</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <table dir="rtl" width={626} cellPadding={7} cellSpacing={0}>
              <colgroup>
                <col width={610} />
              </colgroup>
              <tbody>
                <tr>
                  <td
                    width={610}
                    height={173}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              شهروند گرامی، از شما تقاضا دارم چنانچه فکر می کنید عامل یا
                              عوامل موثر دیگری بر انتخاب گونه های حمل و نقلی مناسب در بافت
                              تاریخی وجود دارند که در جدول فوق از قلم افتاده اند، لطفا در
                              این قسمت مرقوم فرمائید

                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={3} style={{ fontSize: "12pt" }}>
                        <span lang="en-US">.</span>
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              با تشکر
                                            {this.renderBigTextInput("comment")}
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={3} style={{ fontSize: "12pt" }}>
                        <span lang="en-US">.</span>
                      </font>
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
            <p
              lang="en-US"
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <a name="_GoBack" />
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              
            </p>
            <p align="right" style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "16pt" }}>
                      <b>نحوه پاسخگویی به پرسشنامه</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "16pt" }}>
                <b>:</b>
              </font>
            </p>
            <p
              dir="rtl"
              align="justify"
              style={{ textIndent: "0.5in", marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      همانطور که در جدول زیر مشاهده میکنید، در هر سطر یک گونه حمل‌ونقلی در
                      سمت راست، یکی گونه حمل‌ونقلی در سمت چپ و هم همچنین{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                9
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      مربع خالی در بین این دو معیار وجود دارد
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                .
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      هر دو شاخص موجود در هر سطر بر روی برخی از{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>عوامل موثر در انتخاب گونه‌های حمل‌ونقلی مناسب در بافت تاریخی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">
                  <b>"</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      موثر هستند
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">.</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      از شما پاسخ دهنده عزیز تقاضا دارم، با امتیازدهی مناسب از{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">1</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      تا
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">9</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      یا
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">1-</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      تا
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">9-</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      به شاخص‌های موجود در هر سطر میزان اثرگذاری آنها را به صورت مقایسه
                      زوجی تعیین نمایید
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">.</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      چنانچه از نظر شما هر دو شاخص مورد نظر اثرگذاری یکسانی دارند، میبایست
                      به آن سطر امتیاز
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">
                  <b>"1"</b>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US"></span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      تعلق گیرد
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">.</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      به عنوان نمونه اگر به نظر شما تاثیر عامل
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">"</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      سازگاری با مشخصات فردی
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">"</span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US"></span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      در ستون سمت راست جدول زیر نسبت به عامل
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">"</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      هزینه تهیه وسیله
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">"</span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US"></span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      کمی بیشتر است، در ردیف مربوطه و درزیر ستون کمی بیشتر{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">(</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      امتیاز
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">3)</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      را علامت بزنید و اگر پاسخ شما{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">"</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      خیلی کمتر
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">"</span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US"></span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      است در ردیف مربوطه زیر ستون خیلی کمتر{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">(7-)</span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      را علامت بزنید
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <span lang="en-US">.</span>
              </font>
            </p>
            <p align="right" style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "16pt" }}>
                      <b>پرسشنامه شماره </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "16pt" }}>
                <b>(1)</b>
              </font>
            </p>
            <p align="right" style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      لطفا پرسشنامه زیر را به دقت مطالعه بفرمایید و مطابق با نظر شخصی خود
                      به آن پاسخ دهید
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                .
              </font>
            </p>
            <p align="right" style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      باتشکر
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                .
              </font>
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginLeft: "0.2in", marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={3} style={{ fontSize: "12pt" }}>
                1-{" "}
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      بنظر شما کدام یک از روش‌های زیر در مقایسه دوبه‌دو از نظر{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>سازگاری با مشخصات فردی شما </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>(</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>سن، جنسیت، محدودیت های فیزیکی و اعتقادی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>)"</b>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}></font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      مناسبتر است؟
                    </font>
                  </font>
                </span>
              </font>
            </p>












































{/* start of extra */}

{/* var NewComponent = React.createClass({
  render: function() {
    return (
 */}
      <center>
        <table dir="rtl" width={702} cellPadding={7} cellSpacing={0}>
          <colgroup><col width={137} />
            <col width={31} />
            <col width={31} />
            <col width={31} />
            <col width={31} />
            <col width={31} />
            <col width={31} />
            <col width={31} />
            <col width={31} />
            <col width={31} />
            <col width={4356} />
            <col width={131} />
            <col width={4356} />
          </colgroup><tbody><tr valign="bottom">
              <td width={137} height={4} style={{background: 'transparent'}}><p dir="ltr">
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td colSpan={10} width={390} style={{background: 'transparent'}}><p align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={6} style={{fontSize: '22pt'}}><b>امتیازات</b></font></font></font></span></font></p>
              </td>
              <td colSpan={2} width={132} style={{background: 'transparent'}}><p dir="ltr">
                  <font color="#000000">&nbsp;</font></p>
              </td>
            </tr>
            <tr>
              <td width={137} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={5} style={{fontSize: '20pt'}}>عوامل
                            موثر</font></font></font></span></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font color="#000000"><font face="Calibri, serif"><font size={4} style={{fontSize: '16pt'}}><b>9</b></font></font></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font color="#000000"><font face="Calibri, serif"><font size={4} style={{fontSize: '16pt'}}><b>7</b></font></font></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font color="#000000"><font face="Calibri, serif"><font size={4} style={{fontSize: '16pt'}}><b>5</b></font></font></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font color="#000000"><font face="Calibri, serif"><font size={4} style={{fontSize: '16pt'}}><b>3</b></font></font></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font color="#000000"><font face="Calibri, serif"><font size={4} style={{fontSize: '16pt'}}><b>1</b></font></font></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font color="#000000"><font face="Calibri, serif"><font size={4} style={{fontSize: '16pt'}}><b>-3</b></font></font></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font color="#000000"><font face="Calibri, serif"><font size={4} style={{fontSize: '16pt'}}><b>-5</b></font></font></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font color="#000000"><font face="Calibri, serif"><font size={4} style={{fontSize: '16pt'}}><b>-7</b></font></font></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font color="#000000"><font face="Calibri, serif"><font size={4} style={{fontSize: '16pt'}}><b>-9</b></font></font></font></p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={5} style={{fontSize: '20pt'}}>عوامل
                            موثر</font></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={36} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} style={{background: 'transparent'}}><p align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={2} style={{fontSize: '9pt'}}><b>کاملا
                              بیشتر</b></font></font></font></span></font></p>
              </td>
              <td width={31} style={{background: 'transparent'}}><p align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={2} style={{fontSize: '9pt'}}><b>خیلی
                              بیشتر</b></font></font></font></span></font></p>
              </td>
              <td width={31} style={{background: 'transparent'}}><p align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={2} style={{fontSize: '9pt'}}><b>بیشتر</b></font></font></font></span></font></p>
              </td>
              <td width={31} style={{background: 'transparent'}}><p align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={2} style={{fontSize: '9pt'}}><b>کمی
                              بیشتر</b></font></font></font></span></font></p>
              </td>
              <td width={31} style={{background: 'transparent'}}><p align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={2} style={{fontSize: '9pt'}}><b>یکسان</b></font></font></font></span></font></p>
              </td>
              <td width={31} style={{background: 'transparent'}}><p align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={2} style={{fontSize: '9pt'}}><b>کمی
                              کمتر</b></font></font></font></span></font></p>
              </td>
              <td width={31} style={{background: 'transparent'}}><p align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={2} style={{fontSize: '9pt'}}><b>کمتر</b></font></font></font></span></font></p>
              </td>
              <td width={31} style={{background: 'transparent'}}><p align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={2} style={{fontSize: '9pt'}}><b>خیلی
                              کمتر</b></font></font></font></span></font></p>
              </td>
              <td width={31} style={{background: 'transparent'}}><p align="center">
                  <font face="Arial"><span lang="ar-SA"><font color="#000000"><font face="B Nazanin"><font size={2} style={{fontSize: '9pt'}}><b>کاملا
                              کمتر</b></font></font></font></span></font></p>
              </td>
              <td colSpan={2} width={132} style={{background: 'transparent'}}><p dir="ltr">
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p align="justify">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگاری
                            با مشخصات فردی </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سن،
                            جنسیت، محدودیت های فیزیکی و اعتقادی</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_1", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_1", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_1", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_1", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_1", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_1", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_1", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_1", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_1", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            تهیه وسیله </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>خرید
                            وسیله نقلیه اگر شخصی باشد</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگاری
                            با مشخصات فردی </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سن،
                            جنسیت، محدودیت های فیزیکی و اعتقادی</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_2", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_2", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_2", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_2", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_2", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_2", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_2", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_2", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_2", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            استفاده از وسیله شخصی یا کرایه وسیله
                            همگانی و اشتراکی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگاری
                            با مشخصات فردی </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سن،
                            جنسیت، محدودیت های فیزیکی و اعتقادی</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_3", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_3", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_3", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_3", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_3", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_3", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_3", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_3", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_3", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right" style={{marginLeft: '0.25in', marginBottom: '0.11in'}}>
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>زمان
                            سفر </b></font></font></span></font>
                </p>
                <p dir="ltr"><br />
                </p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={6} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگاری
                            با مشخصات فردی </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سن،
                            جنسیت، محدودیت های فیزیکی و اعتقادی</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_4", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_4", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_4", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_4", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_4", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_4", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_4", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_4", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_4", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>راحتی
                            استفاده </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سادگی
                            استفاده، سهولت دسترسی، تناسب با اقلیم،
                            تعداد تغییر شیوه سفر</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگاری
                            با مشخصات فردی </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سن،
                            جنسیت، محدودیت های فیزیکی و اعتقادی</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_5", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_5", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_5", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_5", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_5", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_5", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_5", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_5", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_5", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگار
                            بودن با پایداری محیط زیست و سلامت فردی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={6} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگاری
                            با مشخصات فردی </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سن،
                            جنسیت، محدودیت های فیزیکی و اعتقادی</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_6", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_6", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_6", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_6", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_6", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_6", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_6", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_6", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_6", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right" style={{marginBottom: '0.11in'}}>
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هماهنگی
                            با ساختار فیزیکی و ویژگیهای معماری
                            بافت تاریخی</b></font></font></span></font></p>
                <p dir="ltr"><br />
                </p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="right" style={{marginLeft: '0.25in'}}>
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            تهیه وسیله </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>خرید
                            وسیله نقلیه اگر شخصی باشد</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_7", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_7", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_7", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_7", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_7", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_7", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_7", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_7", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_7", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            استفاده از وسیله اگر به صورت اشتراکی
                          </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>کرایه
                            ای</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)
                    </b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>استفاده
                            شود </b></font></font></span></font>
                </p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="right" style={{marginLeft: '0.25in'}}>
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            تهیه وسیله </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>خرید
                            وسیله نقلیه اگر شخصی باشد</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_8", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_8", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_8", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_8", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_8", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_8", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_8", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_8", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_8", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right" style={{marginLeft: '0.25in', marginBottom: '0.11in'}}>
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>زمان
                            سفر </b></font></font></span></font>
                </p>
                <p dir="ltr"><br />
                </p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            تهیه وسیله </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>خرید
                            وسیله نقلیه اگر شخصی باشد</b></font></font></span></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_9", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_9", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_9", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_9", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_9", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_9", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_9", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_9", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_9", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>راحتی
                            استفاده </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سادگی
                            استفاده، سهولت دسترسی، تناسب با اقلیم،
                            تعداد تغییر شیوه سفر</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            تهیه وسیله </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>خرید
                            وسیله نقلیه اگر شخصی باشد</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_10", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_10", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_10", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_10", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_10", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_10", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_10", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_10", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_10", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگار
                            بودن با پایداری محیط زیست و سلامت فردی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={6} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            تهیه وسیله </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>خرید
                            وسیله نقلیه اگر شخصی باشد</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_11", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_11", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_11", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_11", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_11", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_11", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_11", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_11", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_11", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هماهنگی
                            با ساختار فیزیکی و ویژگیهای معماری
                            بافت تاریخی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            استفاده از وسیله اگر به صورت اشتراکی
                          </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>کرایه
                            ای</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)
                    </b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>استفاده
                            شود </b></font></font></span></font>
                </p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_12", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_12", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_12", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_12", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_12", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_12", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_12", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_12", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_12", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right" style={{marginLeft: '0.25in', marginBottom: '0.11in'}}>
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>زمان
                            سفر </b></font></font></span></font>
                </p>
                <p dir="ltr"><br />
                </p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={6} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            استفاده از وسیله اگر به صورت اشتراکی
                          </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>کرایه
                            ای</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)
                    </b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>استفاده
                            شود </b></font></font></span></font>
                </p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_13", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_13", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_13", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_13", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_13", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_13", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_13", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_13", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_13", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>راحتی
                            استفاده </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سادگی
                            استفاده، سهولت دسترسی، تناسب با اقلیم،
                            تعداد تغییر شیوه سفر</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            استفاده از وسیله اگر به صورت اشتراکی
                          </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>کرایه
                            ای</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)
                    </b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>استفاده
                            شود </b></font></font></span></font>
                </p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_14", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_14", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_14", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_14", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_14", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_14", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_14", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_14", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_14", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگار
                            بودن با پایداری محیط زیست و سلامت فردی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p dir="ltr" align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هزینه
                            استفاده از وسیله اگر به صورت اشتراکی
                          </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>کرایه
                            ای</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)
                    </b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>استفاده
                            شود </b></font></font></span></font>
                </p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_15", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_15", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_15", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_15", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_15", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_15", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_15", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_15", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_15", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هماهنگی
                            با ساختار فیزیکی و ویژگیهای معماری
                            بافت تاریخی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p align="center" style={{marginLeft: '0.25in'}}>
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>زمان
                            سفر</b></font></font></span></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_16", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_16", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_16", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_16", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_16", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_16", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_16", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_16", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_16", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>راحتی
                            استفاده </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سادگی
                            استفاده، سهولت دسترسی، تناسب با اقلیم،
                            تعداد تغییر شیوه سفر</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={6} valign="bottom" style={{background: 'transparent'}}><p align="center" style={{marginLeft: '0.25in', marginBottom: '0.11in'}}>
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>زمان
                            سفر</b></font></font></span></font></p>
                <p dir="ltr" align="center"><br />
                </p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_17", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_17", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_17", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_17", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_17", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_17", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_17", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_17", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_17", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="justify">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگار
                            بودن با پایداری محیط زیست و سلامت فردی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p align="center" style={{marginLeft: '0.25in', marginBottom: '0.11in'}}>
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>زمان
                            سفر</b></font></font></span></font></p>
                <p dir="ltr" align="center"><br />
                </p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_18", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_18", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_18", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_18", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_18", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_18", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_18", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_18", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_18", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هماهنگی
                            با ساختار فیزیکی و ویژگیهای معماری
                            بافت تاریخی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>راحتی
                            استفاده </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سادگی
                            استفاده، سهولت دسترسی، تناسب با اقلیم،
                            تعداد تغییر شیوه سفر</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_19", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_19", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_19", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_19", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_19", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_19", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_19", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_19", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_19", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگار
                            بودن با پایداری محیط زیست و سلامت فردی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>راحتی
                            استفاده </b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>(</b></font><font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سادگی
                            استفاده، سهولت دسترسی، تناسب با اقلیم،
                            تعداد تغییر شیوه سفر</b></font></font></span></font><font size={2} style={{fontSize: '10pt'}}><b>)</b></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_20", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_20", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_20", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_20", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_20", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_20", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_20", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_20", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_20", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هماهنگی
                            با ساختار فیزیکی و ویژگیهای معماری
                            بافت تاریخی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
            <tr>
              <td width={137} height={5} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>سازگار
                            بودن با پایداری محیط زیست و سلامت فردی</b></font></font></span></font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
                                          {this.renderRadio("tableExtra_21", "9")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_21", "7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_21", "5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_21", "3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_21", "1")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_21", "-3")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_21", "-5")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_21", "-7")}
                  <font color="#000000">&nbsp;</font></p>
              </td>
              <td width={31} valign="bottom" style={{background: 'transparent'}}><p dir="ltr">
              {this.renderRadio("tableExtra_21", "-9")}
                  <br />
                </p>
              </td>
              <td colSpan={2} width={132} valign="bottom" style={{background: 'transparent'}}><p align="right">
                  <font face="Arial"><span lang="ar-SA"><font face="B Nazanin"><font size={2} style={{fontSize: '10pt'}}><b>هماهنگی
                            با ساختار فیزیکی و ویژگیهای معماری
                            بافت تاریخی</b></font></font></span></font></p>
              </td>
              <td width={4356} valign="top" style={{border: 'none', padding: '0in'}}><p dir="ltr">
                  <br />
                </p>
              </td>
            </tr>
          </tbody></table>
      </center>













{/* end of extra */}































{/* end of extra */}




















            
            <center>
              <table dir="rtl" width={662} cellPadding={7} cellSpacing={0}>
                <colgroup>
                  <col width={122} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={24} />
                  <col width={25} />
                  <col width={25} />
                  <col width={163} />
                </colgroup>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={23} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td colSpan={9} width={333} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={6} style={{ fontSize: "22pt" }}>
                                  <b>امتیازات</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={5} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>1</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td
                      width={122}
                      height={69}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  یکسان
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 94244"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("pedalBikeElectricBike", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricBike", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricBike", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricBike", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricBike", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricBike", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricBike", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricBike", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricBike", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 94245"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 94246"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("pedalBikeFoldingPedalBike", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeFoldingPedalBike", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeFoldingPedalBike", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeFoldingPedalBike", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeFoldingPedalBike", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeFoldingPedalBike", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeFoldingPedalBike", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeFoldingPedalBike", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeFoldingPedalBike", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 94247"
                              align="bottom"
                              width={24}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 94248"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("pedalBikeElectricMotor", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricMotor", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricMotor", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricMotor", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricMotor", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricMotor", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricMotor", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricMotor", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("pedalBikeElectricMotor", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 94249"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 94250"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q4", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q4", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q4", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q4", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q4", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q4", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q4", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q4", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q4", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center" style={{ marginBottom: "0in" }}>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}></font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                      <p dir="ltr" align="center">
                        <img
                          src={img29}
                          name="Picture 94251"
                          align="bottom"
                          width={18}
                          height={19}
                          border={0}
                        />
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 94252"
                              align="bottom"
                              width={32}
                              height={32}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q5", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q5", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q5", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q5", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q5", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q5", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q5", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q5", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q5", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117900"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 94254"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q6", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q6", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q6", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q6", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q6", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q6", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q6", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q6", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q6", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                دوچرخه تاشو پدالی
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 94255"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 94256"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q7", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q7", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q7", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q7", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q7", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q7", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q7", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q7", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q7", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 94257"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 94258"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q8", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q8", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q8", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q8", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q8", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q8", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q8", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q8", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q8", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 94259"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 94260"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q9", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q9", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q9", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q9", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q9", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q9", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q9", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q9", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q9", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117899"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 94262"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q10", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q10", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q10", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q10", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q10", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q10", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q10", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q10", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q10", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 94263"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 94264"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q11", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q11", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q11", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q11", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q11", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q11", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q11", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q11", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q11", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 94265"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 94266"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q12", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q12", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q12", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q12", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q12", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q12", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q12", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q12", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q12", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117898"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 94268"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q13", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q13", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q13", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q13", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q13", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q13", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q13", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q13", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q13", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 94269"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 94270"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q14", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q14", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q14", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q14", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q14", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q14", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q14", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q14", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q14", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117897"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 112640"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table1_q15", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q15", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q15", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q15", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q15", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q15", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q15", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q15", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table1_q15", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117896"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </center>
            <p align="right" style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <font size={3} style={{ fontSize: "12pt" }}>
                2-
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      بنظر شما کدام یک از روش‌های زیر در مقایسه دوبه‌دو از نظر{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>هزینه تهیه وسیله </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>(</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>توانائی خرید وسیله نقلیه اگر شخصی باشد</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>)"</b>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}></font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      مناسبتر است؟
                    </font>
                  </font>
                </span>
              </font>
            </p>
            <center>
              <table dir="rtl" width={662} cellPadding={7} cellSpacing={0}>
                <colgroup>
                  <col width={122} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={24} />
                  <col width={25} />
                  <col width={25} />
                  <col width={163} />
                </colgroup>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={23} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td colSpan={9} width={333} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={6} style={{ fontSize: "22pt" }}>
                                  <b>امتیازات</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={5} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>1</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td
                      width={122}
                      height={69}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  یکسان
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117901"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q1", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q1", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q1", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q1", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q1", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q1", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q1", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q1", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q1", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117902"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117903"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q2", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q2", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q2", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q2", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q2", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q2", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q2", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q2", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q2", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117904"
                              align="bottom"
                              width={24}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117905"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q3", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q3", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q3", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q3", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q3", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q3", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q3", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q3", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q3", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117906"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117907"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q4", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q4", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q4", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q4", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q4", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q4", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q4", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q4", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q4", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center" style={{ marginBottom: "0in" }}>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}></font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                      <p dir="ltr" align="center">
                        <img
                          src={img29}
                          name="Picture 117908"
                          align="bottom"
                          width={18}
                          height={19}
                          border={0}
                        />
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117909"
                              align="bottom"
                              width={32}
                              height={32}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q5", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q5", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q5", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q5", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q5", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q5", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q5", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q5", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q5", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117910"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117911"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q6", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q6", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q6", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q6", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q6", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q6", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q6", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q6", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q6", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                دوچرخه تاشو پدالی
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117912"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117913"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q7", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q7", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q7", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q7", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q7", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q7", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q7", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q7", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q7", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117914"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117915"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q8", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q8", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q8", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q8", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q8", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q8", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q8", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q8", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q8", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117916"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117917"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q9", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q9", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q9", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q9", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q9", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q9", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q9", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q9", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q9", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117918"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117919"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q10", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q10", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q10", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q10", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q10", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q10", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q10", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q10", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q10", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117920"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117921"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q11", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q11", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q11", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q11", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q11", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q11", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q11", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q11", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q11", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117922"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117923"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q12", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q12", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q12", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q12", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q12", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q12", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q12", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q12", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q12", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117924"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117925"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q13", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q13", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q13", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q13", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q13", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q13", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q13", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q13", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q13", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117926"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117927"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q14", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q14", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q14", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q14", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q14", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q14", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q14", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q14", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q14", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117928"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117929"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table2_q15", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q15", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q15", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q15", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q15", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q15", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q15", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q15", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table2_q15", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117930"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </center>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              3
              <font size={3} style={{ fontSize: "12pt" }}>
                -{" "}
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      بنظر شما کدام یک از روش‌های زیر در مقایسه دوبه‌دو از نظر{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>هزینه استفاده از وسیله شخصی یا کرایه وسیله همگانی و اشتراکی </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}></font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      مناسبتر است؟
                    </font>
                  </font>
                </span>
              </font>
            </p>
            <center>
              <table dir="rtl" width={662} cellPadding={7} cellSpacing={0}>
                <colgroup>
                  <col width={122} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={24} />
                  <col width={25} />
                  <col width={25} />
                  <col width={163} />
                </colgroup>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={23} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td colSpan={9} width={333} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={6} style={{ fontSize: "22pt" }}>
                                  <b>امتیازات</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={5} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>1</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td
                      width={122}
                      height={69}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  یکسان
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117931"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q1", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q1", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q1", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q1", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q1", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q1", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q1", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q1", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q1", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117932"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117933"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q2", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q2", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q2", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q2", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q2", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q2", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q2", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q2", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q2", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117934"
                              align="bottom"
                              width={24}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117935"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q3", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q3", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q3", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q3", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q3", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q3", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q3", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q3", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q3", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117936"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117937"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q4", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q4", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q4", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q4", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q4", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q4", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q4", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q4", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q4", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center" style={{ marginBottom: "0in" }}>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}></font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                      <p dir="ltr" align="center">
                        <img
                          src={img29}
                          name="Picture 117938"
                          align="bottom"
                          width={18}
                          height={19}
                          border={0}
                        />
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117939"
                              align="bottom"
                              width={32}
                              height={32}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q5", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q5", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q5", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q5", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q5", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q5", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q5", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q5", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q5", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117940"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117941"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q6", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q6", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q6", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q6", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q6", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q6", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q6", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q6", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q6", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                دوچرخه تاشو پدالی
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117942"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117943"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q7", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q7", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q7", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q7", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q7", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q7", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q7", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q7", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q7", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117944"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117945"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q8", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q8", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q8", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q8", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q8", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q8", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q8", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q8", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q8", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117946"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117947"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q9", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q9", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q9", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q9", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q9", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q9", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q9", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q9", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q9", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117948"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117949"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q10", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q10", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q10", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q10", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q10", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q10", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q10", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q10", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q10", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117950"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117951"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q11", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q11", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q11", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q11", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q11", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q11", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q11", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q11", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q11", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117952"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117953"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q12", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q12", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q12", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q12", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q12", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q12", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q12", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q12", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q12", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117954"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117955"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q13", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q13", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q13", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q13", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q13", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q13", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q13", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q13", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q13", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117956"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117957"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q14", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q14", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q14", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q14", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q14", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q14", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q14", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q14", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q14", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117958"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117959"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table3_q15", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q15", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q15", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q15", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q15", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q15", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q15", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q15", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table3_q15", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117960"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </center>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={3} style={{ fontSize: "12pt" }}>
                4-{" "}
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      بنظر شما کدام یک از روش‌های زیر در مقایسه دوبه‌دو از نظر{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>زمان سفر </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      مناسبتر است؟
                    </font>
                  </font>
                </span>
              </font>
            </p>
            <center>
              <table dir="rtl" width={662} cellPadding={7} cellSpacing={0}>
                <colgroup>
                  <col width={122} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={24} />
                  <col width={25} />
                  <col width={25} />
                  <col width={163} />
                </colgroup>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={23} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td colSpan={9} width={333} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={6} style={{ fontSize: "22pt" }}>
                                  <b>امتیازات</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={5} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>1</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td
                      width={122}
                      height={69}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  یکسان
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117961"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q1", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q1", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q1", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q1", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q1", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q1", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q1", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q1", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q1", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117962"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117963"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q2", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q2", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q2", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q2", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q2", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q2", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q2", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q2", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q2", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117964"
                              align="bottom"
                              width={24}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117965"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q3", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q3", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q3", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q3", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q3", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q3", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q3", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q3", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q3", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117966"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117967"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q4", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q4", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q4", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q4", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q4", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q4", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q4", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q4", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q4", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center" style={{ marginBottom: "0in" }}>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}></font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                      <p dir="ltr" align="center">
                        <img
                          src={img29}
                          name="Picture 117968"
                          align="bottom"
                          width={18}
                          height={19}
                          border={0}
                        />
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117969"
                              align="bottom"
                              width={32}
                              height={32}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q5", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q5", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q5", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q5", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q5", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q5", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q5", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q5", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q5", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117970"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117971"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q6", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q6", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q6", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q6", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q6", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q6", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q6", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q6", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q6", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                دوچرخه تاشو پدالی
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117972"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117973"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q7", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q7", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q7", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q7", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q7", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q7", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q7", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q7", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q7", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117974"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117975"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q8", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q8", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q8", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q8", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q8", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q8", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q8", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q8", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q8", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117976"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117977"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q9", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q9", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q9", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q9", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q9", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q9", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q9", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q9", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q9", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117978"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117979"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q10", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q10", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q10", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q10", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q10", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q10", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q10", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q10", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q10", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117980"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117981"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q11", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q11", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q11", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q11", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q11", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q11", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q11", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q11", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q11", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117982"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117983"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q12", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q12", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q12", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q12", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q12", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q12", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q12", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q12", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q12", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117984"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117985"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q13", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q13", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q13", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q13", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q13", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q13", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q13", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q13", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q13", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117986"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117987"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q14", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q14", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q14", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q14", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q14", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q14", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q14", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q14", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q14", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117988"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 117989"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table4_q15", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q15", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q15", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q15", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q15", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q15", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q15", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q15", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table4_q15", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 117990"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </center>
            <p align="right" style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <br />
              <br />
            </p>
            <p align="right" style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <font size={3} style={{ fontSize: "12pt" }}>
                5-
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      بنظر شما کدام یک از روش‌های زیر در مقایسه دوبه‌دو از نظر{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>راحتی استفاده </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>(</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>
                        سادگی استفاده، سهولت دسترسی، تناسب با اقلیم، تعداد تغییر شیوه سفر
                      </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>) "</b>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                {" "}
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      مناسبتر است؟
                    </font>
                  </font>
                </span>
              </font>
            </p>
            <center>
              <table dir="rtl" width={662} cellPadding={7} cellSpacing={0}>
                <colgroup>
                  <col width={122} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={24} />
                  <col width={25} />
                  <col width={25} />
                  <col width={163} />
                </colgroup>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={23} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td colSpan={9} width={333} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={6} style={{ fontSize: "22pt" }}>
                                  <b>امتیازات</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={5} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>1</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td
                      width={122}
                      height={69}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  یکسان
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117991"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q1", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q1", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q1", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q1", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q1", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q1", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q1", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q1", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q1", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 117992"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117993"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q2", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q2", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q2", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q2", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q2", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q2", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q2", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q2", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q2", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 117994"
                              align="bottom"
                              width={24}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117995"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q3", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q3", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q3", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q3", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q3", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q3", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q3", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q3", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q3", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 117996"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117997"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q4", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q4", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q4", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q4", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q4", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q4", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q4", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q4", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q4", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center" style={{ marginBottom: "0in" }}>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}></font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                      <p dir="ltr" align="center">
                        <img
                          src={img29}
                          name="Picture 117998"
                          align="bottom"
                          width={18}
                          height={19}
                          border={0}
                        />
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 117999"
                              align="bottom"
                              width={32}
                              height={32}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q5", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q5", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q5", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q5", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q5", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q5", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q5", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q5", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q5", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118000"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118001"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q6", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q6", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q6", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q6", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q6", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q6", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q6", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q6", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q6", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                دوچرخه تاشو پدالی
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118002"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118003"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q7", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q7", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q7", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q7", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q7", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q7", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q7", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q7", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q7", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118004"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118005"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q8", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q8", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q8", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q8", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q8", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q8", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q8", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q8", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q8", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118006"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118007"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q9", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q9", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q9", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q9", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q9", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q9", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q9", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q9", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q9", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118008"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118009"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q10", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q10", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q10", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q10", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q10", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q10", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q10", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q10", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q10", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118010"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118011"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q11", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q11", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q11", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q11", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q11", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q11", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q11", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q11", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q11", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118012"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118013"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q12", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q12", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q12", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q12", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q12", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q12", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q12", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q12", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q12", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118014"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118015"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q13", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q13", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q13", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q13", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q13", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q13", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q13", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q13", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q13", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118016"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118017"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q14", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q14", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q14", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q14", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q14", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q14", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q14", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q14", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q14", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118018"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118019"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table5_q15", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q15", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q15", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q15", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q15", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q15", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q15", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q15", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table5_q15", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118020"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </center>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={3} style={{ fontSize: "12pt" }}>
                6-{" "}
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      بنظر شما کدام یک از روش‌های زیر در مقایسه دوبه‌دو از نظر{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>سازگار بودن با پایداری محیط زیست و سلامت فردی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      مناسبتر است؟
                    </font>
                  </font>
                </span>
              </font>
            </p>
            <center>
              <table dir="rtl" width={662} cellPadding={7} cellSpacing={0}>
                <colgroup>
                  <col width={122} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={24} />
                  <col width={25} />
                  <col width={25} />
                  <col width={163} />
                </colgroup>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={23} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td colSpan={9} width={333} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={6} style={{ fontSize: "22pt" }}>
                                  <b>امتیازات</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={5} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>1</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td
                      width={122}
                      height={69}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  یکسان
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 118021"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q1", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q1", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q1", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q1", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q1", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q1", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q1", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q1", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q1", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118022"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 118023"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q2", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q2", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q2", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q2", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q2", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q2", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q2", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q2", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q2", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118024"
                              align="bottom"
                              width={24}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 118025"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q3", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q3", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q3", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q3", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q3", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q3", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q3", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q3", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q3", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118026"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 118027"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q4", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q4", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q4", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q4", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q4", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q4", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q4", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q4", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q4", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center" style={{ marginBottom: "0in" }}>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}></font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                      <p dir="ltr" align="center">
                        <img
                          src={img29}
                          name="Picture 118028"
                          align="bottom"
                          width={18}
                          height={19}
                          border={0}
                        />
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 118029"
                              align="bottom"
                              width={32}
                              height={32}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q5", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q5", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q5", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q5", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q5", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q5", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q5", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q5", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q5", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118030"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118031"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q6", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q6", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q6", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q6", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q6", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q6", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q6", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q6", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q6", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                دوچرخه تاشو پدالی
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118032"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118033"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q7", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q7", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q7", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q7", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q7", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q7", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q7", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q7", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q7", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118034"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118035"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q8", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q8", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q8", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q8", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q8", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q8", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q8", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q8", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q8", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118036"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118037"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q9", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q9", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q9", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q9", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q9", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q9", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q9", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q9", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q9", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118038"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118039"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q10", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q10", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q10", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q10", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q10", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q10", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q10", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q10", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q10", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118040"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118041"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q11", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q11", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q11", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q11", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q11", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q11", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q11", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q11", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q11", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118042"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118043"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q12", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q12", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q12", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q12", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q12", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q12", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q12", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q12", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q12", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118044"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118045"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q13", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q13", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q13", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q13", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q13", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q13", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q13", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q13", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q13", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118046"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118047"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q14", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q14", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q14", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q14", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q14", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q14", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q14", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q14", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q14", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118048"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118049"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table6_q15", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q15", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q15", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q15", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q15", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q15", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q15", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q15", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table6_q15", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118050"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </center>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={3} style={{ fontSize: "12pt" }}>
                7-{" "}
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      بنظر شما کدام یک از روش‌های زیر در مقایسه دوبه‌دو از نظر{" "}
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      <b>هماهنگی با ساختار فیزیکی و ویژگیهای معماری بافت تاریخی </b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={3} style={{ fontSize: "12pt" }}>
                <b>"</b>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={3} style={{ fontSize: "12pt" }}>
                      مناسبتر است؟
                    </font>
                  </font>
                </span>
              </font>
            </p>
            <center>
              <table dir="rtl" width={662} cellPadding={7} cellSpacing={0}>
                <colgroup>
                  <col width={122} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={25} />
                  <col width={24} />
                  <col width={25} />
                  <col width={25} />
                  <col width={163} />
                </colgroup>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={23} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td colSpan={9} width={333} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={6} style={{ fontSize: "22pt" }}>
                                  <b>امتیازات</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={5} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>1</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-3</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-5</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-7</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font color="#000000">
                          <font face="Calibri, serif">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              <b>-9</b>
                            </font>
                          </font>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  <b>گونه حمل‌ونقلی</b>
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td
                      width={122}
                      height={69}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی مناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  یکسان
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کمی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  خیلی نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={3} style={{ fontSize: "12pt" }}>
                                  کاملا نامناسب تر
                                </font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr">
                        <font color="#000000">&nbsp;</font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 118051"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q1", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q1", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q1", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q1", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q1", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q1", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q1", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q1", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q1", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118052"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 118053"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q2", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q2", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q2", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q2", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q2", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q2", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q2", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q2", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q2", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118054"
                              align="bottom"
                              width={24}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 118055"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q3", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q3", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q3", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q3", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q3", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q3", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q3", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q3", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q3", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118056"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 118057"
                              align="bottom"
                              width={33}
                              height={33}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q4", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q4", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q4", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q4", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q4", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q4", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q4", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q4", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q4", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center" style={{ marginBottom: "0in" }}>
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}></font>
                              </font>
                            </font>
                          </span>
                        </font>
                      </p>
                      <p dir="ltr" align="center">
                        <img
                          src={img29}
                          name="Picture 118058"
                          align="bottom"
                          width={18}
                          height={19}
                          border={0}
                        />
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img2}
                              name="Picture 118059"
                              align="bottom"
                              width={32}
                              height={32}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q5", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q5", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q5", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q5", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q5", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q5", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q5", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q5", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q5", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118060"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118061"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q6", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q6", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q6", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q6", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q6", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q6", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q6", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q6", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q6", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                دوچرخه تاشو پدالی
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118062"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118063"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q7", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q7", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q7", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q7", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q7", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q7", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q7", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q7", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q7", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118064"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={24}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118065"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q8", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q8", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q8", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q8", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q8", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q8", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q8", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q8", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q8", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118066"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img31}
                              name="Picture 118067"
                              align="bottom"
                              width={32}
                              height={21}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q9", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q9", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q9", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q9", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q9", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q9", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q9", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q9", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q9", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118068"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118069"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q10", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q10", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q10", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q10", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q10", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q10", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q10", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q10", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q10", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                اسکوتر برقی
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118070"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118071"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q11", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q11", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q11", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q11", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q11", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q11", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q11", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q11", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q11", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                موتورسیکلت برقی کم سرعت شهری
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118072"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}></font>
                            </font>
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr>
                    <td
                      width={122}
                      height={25}
                      valign="bottom"
                      style={{ background: "transparent" }}
                    >
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  دوچرخه تاشو پدالی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img27}
                              name="Picture 118073"
                              align="bottom"
                              width={22}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q12", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q12", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q12", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q12", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q12", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q12", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q12", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q12", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q12", "-9")}
                        <br />
                      </p>
                    </td>

                    <td width={163} valign="top" style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font face="B Nazanin">
                              <font size={2} style={{ fontSize: "10pt" }}>
                                خودروهای سه چرخ و چهار چرخ برقی کوچک
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118074"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118075"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q13", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q13", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q13", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q13", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q13", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q13", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q13", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q13", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q13", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118076"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  اسکوتر برقی
                                </font>
                              </font>
                            </font>
                            <img
                              src={img28}
                              name="Picture 118077"
                              align="bottom"
                              width={26}
                              height={27}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q14", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q14", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q14", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q14", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q14", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q14", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q14", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q14", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q14", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118078"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
                <tbody>
                  <tr valign="bottom">
                    <td width={122} height={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  موتورسیکلت برقی کم سرعت شهری
                                </font>
                              </font>
                            </font>
                            <img
                              src={img29}
                              name="Picture 118079"
                              align="bottom"
                              width={19}
                              height={20}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        {this.renderRadio("table7_q15", "9")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q15", "7")}

                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q15", "5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q15", "3")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q15", "1")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q15", "-3")}
                        <br />
                      </p>
                    </td>
                    <td width={24} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q15", "-5")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q15", "-7")}
                        <br />
                      </p>
                    </td>
                    <td width={25} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                      {this.renderRadio("table7_q15", "-9")}
                        <br />
                      </p>
                    </td>
                    <td width={163} style={{ background: "transparent" }}>
                      <p dir="ltr" align="center">
                        <font face="Arial">
                          <span lang="fa-IR">
                            <font color="#000000">
                              <font face="B Nazanin">
                                <font size={2} style={{ fontSize: "10pt" }}>
                                  خودروهای سه چرخ و چهار چرخ برقی کوچک
                                </font>
                              </font>
                            </font>
                            <img
                              src={img30}
                              name="Picture 118080"
                              align="bottom"
                              width={23}
                              height={19}
                              border={0}
                            />
                          </span>
                        </font>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </center>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="justify"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>(</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>ه</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>)-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>نظرات تکمیلی و پیشنهادهای فرد مورد پرسش</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <table dir="rtl" width={626} cellPadding={7} cellSpacing={0}>
              <colgroup>
                <col width={610} />
              </colgroup>
              <tbody>
                <tr>
                  <td
                    width={610}
                    height={173}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="justify">
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              شهروند گرامی، از شما تقاضا دارم چنانچه فکر می کنید گونه یا
                              گونه های حمل و نقلی مناسب دیگری برای استفاده در بافت تاریخی
                              وجود دارند که در جدول فوق از قلم افتاده اند، لطفا در این
                              قسمت مرقوم فرمائید
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={4} style={{ fontSize: "14pt" }}>
                        <span lang="en-US">.</span>
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={4} style={{ fontSize: "14pt" }}>
                              با تشکر
                                                      {this.renderBigTextInput("lastComment")}
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={4} style={{ fontSize: "14pt" }}>
                        <span lang="en-US">.</span>
                      </font>
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
            <p
              lang="en-US"
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>(</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>ی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>)-</b>
                </span>
              </font>
              <font face="Arial">
                <span lang="fa-IR">
                  <font face="B Nazanin">
                    <font size={4} style={{ fontSize: "14pt" }}>
                      <b>اولویت‌بندی گونه‌های حمل‌ونقلی</b>
                    </font>
                  </font>
                </span>
              </font>
              <font size={4} style={{ fontSize: "14pt" }}>
                <span lang="en-US">
                  <b>:</b>
                </span>
              </font>
            </p>
            <table dir="rtl" width={610} cellPadding={7} cellSpacing={0}>
              <colgroup>
                <col width={594} />
              </colgroup>
              <tbody>
                <tr>
                  <td
                    width={594}
                    height={291}
                    valign="top"
                    style={{ border: "1px solid #000000", padding: "0in 0.08in" }}
                  >
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              شهروند گرامی، از شما تقاضا دارم طبق نظر شخصی خود برای
                              گونه‌های حمل و نقلی زیر اولویت بندی در نظر گرفته و با ذکر
                              شماره از{" "}
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={3} style={{ fontSize: "12pt" }}>
                        1
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              تا
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={3} style={{ fontSize: "12pt" }}>
                        11 (
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              عدد
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={3} style={{ fontSize: "12pt" }}>
                        1
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              برای بیشترین اولویت و عدد{" "}
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={3} style={{ fontSize: "12pt" }}>
                        11
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              برای کمترین اولویت
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={3} style={{ fontSize: "12pt" }}>
                        )
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              یاداشت بفرمایید
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={3} style={{ fontSize: "12pt" }}>
                        .
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              باتشکر
                            </font>
                          </font>
                        </span>
                      </font>
                      <font size={3} style={{ fontSize: "12pt" }}>
                        .
                      </font>
                    </p>
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font size={3} style={{ fontSize: "12pt" }}>
                                                {this.renderTextInput("lastPedalBike")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              دوجرخه پدالی شخصی
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font size={3} style={{ fontSize: "12pt" }}>
                                                  {this.renderTextInput("lastSharingPedalBike")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              دوجرخه پدالی اشتراکی
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font size={3} style={{ fontSize: "12pt" }}>
                                            {this.renderTextInput("lastElectricBike")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              دوچرخه برقی
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font size={3} style={{ fontSize: "12pt" }}>
                      {this.renderTextInput("lastFoldingBike")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              دوچرخه تاشو
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font size={3} style={{ fontSize: "12pt" }}>
                                                {this.renderTextInput("lastScooter")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              اسکوتر شخصی
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font size={3} style={{ fontSize: "12pt" }}>
                                            {this.renderTextInput("lastSharingScooter")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              اسکوتر اشتراکی
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font size={3} style={{ fontSize: "12pt" }}>
                                        {this.renderTextInput("lastPersonalBikeWithPublicTransportation")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              حمل دوچرخه شخصی با حمل‌ونقل همگانی
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font size={3} style={{ fontSize: "12pt" }}>
                      {this.renderTextInput("lastScooterWithPublicTransportation")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              حمل اسکوتر با حمل‌ونقل همگانی
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font size={3} style={{ fontSize: "12pt" }}>
                      {this.renderTextInput("lastPersonalFoldingBikeWithPublicTransportation")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              حمل دوچرخه تاشو با حمل‌ونقل همگانی
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="right" style={{ marginBottom: "0in" }}>
                      <font size={3} style={{ fontSize: "12pt" }}>
                      {this.renderTextInput("lastThreeWheelCar")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              خودروهای سه‌چرخ و چهار‌چرخ برقی کوچک
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                    <p align="right">
                      <font size={3} style={{ fontSize: "12pt" }}>
                      {this.renderTextInput("lastPublicCarHistoricalPlace(Baft)")}
                      </font>
                      <font face="Arial">
                        <span lang="fa-IR">
                          <font face="B Nazanin">
                            <font size={3} style={{ fontSize: "12pt" }}>
                              خودرو همگانی مخصوص بافت تاریخی
                            </font>
                          </font>
                        </span>
                      </font>
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <font size={3} style={{ fontSize: "12pt" }}>
                {" "}
              </font>
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p
              dir="rtl"
              align="right"
              style={{ marginBottom: "0.11in", lineHeight: "108%" }}
            >
              <br />
              <br />
            </p>
            <p style={{ marginBottom: "0.11in", lineHeight: "108%" }}>
              <br />
              <br />
            </p>
            <div title="footer">
              <p
                align="center"
                style={{ marginTop: "0.47in", marginBottom: "0in", lineHeight: "100%" }}
              >
                <span style={{ background: "#c0c0c0" }}>
                  <sdfield type="PAGE" subtype="RANDOM" format="PAGE">
                    18
                  </sdfield>
                </span>
              </p>
              <p style={{ marginBottom: "0in", lineHeight: "100%" }}>
                <br />
              </p>
            </div>

            {this.renderSubmitButton()}



            </form>



            </div>          


);
}
}


export default Citizen;



