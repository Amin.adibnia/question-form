import React from "react";
import axios from "axios";
import Citizen from "./components/Citizen";


class QuestionForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'نام'
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    
    handleChange(event) {
        this.setState({name: event.target.value});
    }
  
    handleSubmit(event) {
      // axios({
      //   method: 'post',
      //   url: 'http://127.0.0.1:8080/question-form',
      //   data: {
      //     name: 'Fred',
      //   }
      // });
          
    //   fetch(BASE_URL + url, {
    //     method: 'POST',
    //     body: JSON.stringify(data, replacer),
    //     headers: getHeaders(),
    //     // credentials: 'include',
    // })
    //     .then(response => {
    //         const json = response.json(); // there's always a body
    //         if (response.status >= 200 && response.status < 300) {
    //             return json.then(res => {
    //                 return res.result;
    //             });
    //         }
    //         if (response.status >= 502 && response.status <= 504) {
    //             toastr.error('Can not connect to server. Please check your internet connection.');
    //         }
    //         return json.then(Promise.reject.bind(Promise));
    //     })
    //     .catch((err) => {
    //         return handleError(err);
    //     });
        // const params = new URLSearchParams();
        // params.append('name', 'Amin');
        // params.append('param2', 'value2');
        // axios.post('http://localhost:8080/question-form', params);
    
        axios({
            method: "post",
            url: "http://localhost:8080/question-form",
            data: {
               name: this.state.name
              },
            headers: { "Content-Type": "application/json" },
        })
            .then(function (response) {
              //handle success
              alert("ممنون از همکاری شما :)")
            })
            .catch(function (response) {
              //handle error
                console.log(response);
            });
              event.preventDefault();
    }
  
    render() {
        return (
            // <form onSubmit={this.handleSubmit}>
                <Citizen />
                // <label>
                //     نام:
                //     <input value={this.state.name} onChange={this.handleChange} />
                // </label>
                // <input type="submit" value="Submit" />
            // </form>
        );
    }
}


export default QuestionForm;
