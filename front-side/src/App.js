import './App.css';
import Citizen from './components/Citizen';
import QForm from './components/QForm';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";


function App() {
  return (

    <Router>
      <div>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/citizen">
            <Citizen />
          </Route>
          <Route path="/qform">
            <QForm />
          </Route>
          {/* <Route path="/">
            <Home />
          </Route> */}
        </Switch>
      </div>
    </Router>


    // <Router history = {browserHistory}>
    // {/* <Route path = "/" component = {App}> */}

    // <Route path = "/citizen" component = {Citizen} />
    // <Route path = "/qform" component = {QForm} />
    // </Router>


    // <Citizen />
    // <QForm2 />
  );
}

export default App;
